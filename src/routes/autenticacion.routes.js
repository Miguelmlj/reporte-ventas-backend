import { Router } from "express";
import {
    login,
    loginDMSUsers
} from "../controllers/autenticacion.controller";

const router = Router();

router.post("/login", login);
router.post("/login/usuariosDMS", loginDMSUsers)

export default router;
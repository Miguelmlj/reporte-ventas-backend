import { Router } from "express";
import {
    getIndicadorLeads,
    createIndicadorLeads,
    updateIndicadorLeads
} from "../controllers/Leads_indicador.controller";
const router = Router();

router.post("/indicadorLeads/get", getIndicadorLeads)
router.post("/indicadorLeads/create", createIndicadorLeads)
router.patch("/indicadorLeads/update", updateIndicadorLeads)

export default router;
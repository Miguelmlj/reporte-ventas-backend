import { Router } from "express";
import {
    getAccesorios,
    createAccesorios,
    updateAccesorios
} from "../controllers/ctrlOper_Accesorios.controller"

const router = Router();

router.post("/ctrloperAccesorios/get", getAccesorios);
router.post("/ctrloperAccesorios/create", createAccesorios);
router.patch("/ctrloperAccesorios/update", updateAccesorios);

export default router;
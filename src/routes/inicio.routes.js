import { Router } from "express";
import {
    obtenerReporteDiario,
    obtenerAcumuladoDelMes,
    obtenerNombreAgencia
  } from "../controllers/inicio.controller";

  const router = Router();

  router.post("/inicio", obtenerReporteDiario);

  router.post("/inicio/agencia", obtenerNombreAgencia);

  router.post("/acumulado", obtenerAcumuladoDelMes);
  
  export default router;
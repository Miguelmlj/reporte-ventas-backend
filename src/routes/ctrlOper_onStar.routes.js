import { Router } from "express";
import {
    getOnStar,
    createOnStar,
    updateOnStar
} from "../controllers/ctrlOper_onStar.controller"

const router = Router();

router.post("/ctrloperOnstart/get", getOnStar)
router.post("/ctrloperOnstart/create", createOnStar)
router.patch("/ctrloperOnstart/update", updateOnStar)

export default router;
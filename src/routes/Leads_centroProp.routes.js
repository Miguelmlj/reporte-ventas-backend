import { Router } from "express";
import {
    getLeadsCentroProp,
    createLeadsCentroProp,
    updateLeadsCentroProp
} from "../controllers/Leads_centroProp.controller"

const router = Router();

router.post("/LeadsCentroProp/get", getLeadsCentroProp)
router.post("/LeadsCentroProp/create", createLeadsCentroProp)
router.patch("/LeadsCentroProp/update", updateLeadsCentroProp)

export default router;
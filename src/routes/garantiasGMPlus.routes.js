import { Router } from "express";
import {
    getGarantiasGMPlus,
    createGarantiasGMPlus,
    updateGarantiasGMPlus
} from "../controllers/garantiasGMPlus.controller"

const router = Router();

router.post("/garantiasGMPlus/get", getGarantiasGMPlus)
router.post("/garantiasGMPlus/create", createGarantiasGMPlus)
router.patch("/garantiasGMPlus/update", updateGarantiasGMPlus)

export default router;
import { Router } from "express";
import {
    getAfluencia,
    createAfluencia,
    getAfluenciaByFecha,
     updateAfluenciaByFecha
  } from "../controllers/afluencia.controller";

const router = Router();

router.post("/afluencia", getAfluencia);

router.post("/afluenciabyfecha", getAfluenciaByFecha);

router.post("/afluenciacreate", createAfluencia);

 router.patch("/afluenciaupdate", updateAfluenciaByFecha)

export default router;
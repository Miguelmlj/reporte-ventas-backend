import { Router } from "express";

import {
    getGarantiPlus,
    createGarantiPlus,
    updateGarantiPlus
} from "../controllers/ctrlOper_GarantiPlus.controller"

const router = Router();

router.post("/ctrloperGarantiPlus/get", getGarantiPlus);
router.post("/ctrloperGarantiPlus/create", createGarantiPlus);
router.patch("/ctrloperGarantiPlus/update", updateGarantiPlus);

export default router;
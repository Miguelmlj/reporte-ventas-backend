import { Router } from "express";
import {
    getIndicadorMkt,
    createIndicadorMkt,
    updateIndicadorMkt
} from "../controllers/Mkt_indicadores.controller"

const router = Router();

router.post("/indicadorMkt/get", getIndicadorMkt);
router.post("/indicadorMkt/create", createIndicadorMkt);
router.patch("/indicadorMkt/update", updateIndicadorMkt);

export default router;
import { Router } from "express";
import {
    getSolicitudes,
    createSolicitudes,
     getSolicitudesByFecha,
     updateSolicitudesByFecha
  } from "../controllers/solicitudes.controller";

const router = Router();

router.post("/solicitudes", getSolicitudes);

router.post("/solicitudesbyfecha", getSolicitudesByFecha);

router.post("/solicitudescreate", createSolicitudes);

 router.patch("/solicitudesupdate", updateSolicitudesByFecha)

export default router;
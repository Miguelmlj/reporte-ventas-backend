import { Router } from "express";
import {
    tableVehiculosIsEmpty,
    insertAllRowsOfDMSToVehiculos,
    getVehiculosDMS,
    getVehiculosSQL,
    insertVehiculosSQL,
    deleteVehiculosSQL,
    updateVehiculosSQL
} from "../controllers/codigosqr.controller";

const router = Router();

router.get("/codigosqr/tableVehiculosIsEmpty", tableVehiculosIsEmpty);
router.get("/codigosqr/getVehiculosDMS", getVehiculosDMS);
router.get("/codigosqr/getVehiculosSQL", getVehiculosSQL);
router.post("/codigosqr/insertVehiculosSQL", insertVehiculosSQL);
router.delete("/codigosqr/deleteVehiculosSQL", deleteVehiculosSQL);
router.post("/codigosqr/insertAllRowsOfDMSToVehiculos", insertAllRowsOfDMSToVehiculos);

router.post("/codigosqr/updateVehiculosSQL", updateVehiculosSQL);

export default router;


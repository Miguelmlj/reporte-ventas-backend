import { Router } from "express";
import {
    getIndicadorBDC,
    createIndicadorBDC,
    updateIndicadorBDC
} from "../controllers/BDC_indicador.controller"

const router = Router();

router.post("/indicadorBDC/get", getIndicadorBDC);
router.post("/indicadorBDC/create", createIndicadorBDC);
router.patch("/indicadorBDC/update", updateIndicadorBDC);

export default router;
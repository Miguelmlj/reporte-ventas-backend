import { Router } from "express";

import { 
    getInventario, 
    getFechasInventario,
    getInventarioSistema,
    getHistorialComparacionesInventario,
    
    inventarioExisteEnHistorial,
    inventarioCrearEnHistorial,
    inventarioEliminarEnHistorial,
    getInventarioSistemaQr
    /* 
    getInventarioSistemaMochis,
    getInventarioSistemaCadillac,
    getInventarioSistemaCuliacan,
    getInventarioSistemaGuasave 
    */
 } from "../controllers/inventario.controller";

const router = Router();

//ruta para obtener el inventario físico
router.post("/inventario", getInventario);

router.post("/inventarioFechas", getFechasInventario);

//ruta para obtener inventario del sistema actualizado
router.post("/vehiculosdms", getInventarioSistema);

router.post("/vehiculosdmsqr", getInventarioSistemaQr);

//ruta para obtener el historial de comparaciones inventarios
router.post("/inventarioHistorial", getHistorialComparacionesInventario);

//ruta para consultar la existencia de registros en el historial, y 
//dependiendo el resultado  - eliminar ó crear.
router.post("/inventarioExisteEnHistorial", inventarioExisteEnHistorial);
router.post("/inventarioCrearEnHistorial", inventarioCrearEnHistorial);
router.delete("/inventarioEliminarEnHistorial", inventarioEliminarEnHistorial);

export default router;
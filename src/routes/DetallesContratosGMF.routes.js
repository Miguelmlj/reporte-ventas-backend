import { Router } from "express";

import {
    getDetallesContratosGMF,
    createDetallesContratosGMF,
    updateDetallesContratosGMF
} from "../controllers/DetallesContratosGMF.controller"

const router = Router();

router.post("/detallesContratosGMF/get", getDetallesContratosGMF);
router.post("/detallesContratosGMF/create", createDetallesContratosGMF);
router.patch("/detallesContratosGMF/update", updateDetallesContratosGMF);

export default router;
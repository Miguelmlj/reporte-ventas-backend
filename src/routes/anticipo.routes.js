import { Router } from "express"
import {
    getAnticipos,
    createAnticipo,
    getAnticipoById,
    updateAnticipo
} from "../controllers/anticipo.controller";

const router = Router();

router.post("/anticipo", getAnticipos);

router.post("/anticipocreate", createAnticipo);

router.get("/anticipo/:id", getAnticipoById);

router.patch("/anticipo/:id", updateAnticipo);

export default router;
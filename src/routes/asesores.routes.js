import { Router } from "express";
import {getAsesores, getAsesoresActivos} from "../controllers/asesores.controller"

const router = Router();

router.post("/asesores/get", getAsesores);
router.post("/asesores/getactivos", getAsesoresActivos);

export default router;
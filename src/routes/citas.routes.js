import { Router } from "express";
import {
    getCitas,
    createCitas,
    updateCitasByFecha,
    getCitasByFecha
  } from "../controllers/citas.controller";

const router = Router();

router.post("/citas", getCitas);

router.post("/citasbyfecha", getCitasByFecha);

router.post("/citascreate", createCitas);

router.patch("/citasupdate", updateCitasByFecha)

export default router;
import { Router } from "express";
import {
    createIndicadorFYI,
    getIndicadorFYI,
    updateIndicadorFYI
} from "../controllers/FYI_indicador.controller"

const router = Router();

router.post("/indicadorFYI/get", getIndicadorFYI)
router.post("/indicadorFYI/create", createIndicadorFYI)
router.patch("/indicadorFYI/update", updateIndicadorFYI)

export default router;
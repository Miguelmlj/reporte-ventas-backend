import { Router } from "express";
import {
    getGAP,
    createGAP,
    updateGAP
} from "../controllers/ctrlOper_GAP.controller"

const router = Router();

router.post("/ctrloperGAP/get", getGAP)
router.post("/ctrloperGAP/create", createGAP)
router.patch("/ctrloperGAP/update", updateGAP)

export default router;
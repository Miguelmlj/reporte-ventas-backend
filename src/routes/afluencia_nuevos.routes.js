import { Router } from "express";
import {
  getAfluenciaNuevos,
  createAfluenciaNuevos,
//   getProductById,
//   deleteProductById,
//   getTotalProducts,
//   updateProductById,
} from "../controllers/afluencia_nuevos.controller";

const router = Router();

router.get("/afluencianuevos", getAfluenciaNuevos);

router.post("/afluencianuevos", createAfluenciaNuevos);

// router.get("/products/count", getTotalProducts);

// router.get("/products/:id", getProductById);

// router.delete("/products/:id", deleteProductById);

// router.put("/products/:id", updateProductById);

export default router;
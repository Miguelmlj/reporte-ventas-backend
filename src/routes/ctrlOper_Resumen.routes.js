import { Router } from "express"
import { 
    /* getLeasing, 
    getPenetracionGMF,  */
    getUtilidades, 
    getPVAS} from "../controllers/ctrlOper_Resumen.controller"

const router = Router();
/* CONTROL OPERATIVO F&I */
// router.post("/resumen/leasing", getLeasing);
// router.post("/resumen/penetracionGMF", getPenetracionGMF);
router.post("/resumen/utilidades", getUtilidades);

/* DESEMPENO ASESORES */
router.post("/resumen/pvas", getPVAS);


export default router;
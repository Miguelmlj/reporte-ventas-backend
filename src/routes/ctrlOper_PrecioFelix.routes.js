import { Router } from "express";

import {
    getPrecioFelix,
    createPrecioFelix,
    updatePrecioFelix
} from "../controllers/ctrlOper_PrecioFelix.controller"

const router = Router();

router.post("/ctrloperPrecioFelix/get", getPrecioFelix);
router.post("/ctrloperPrecioFelix/create", createPrecioFelix);
router.patch("/ctrloperPrecioFelix/update", updatePrecioFelix);

export default router;
import { Router } from "express";
import {
    getSeguroGMF,
    createSeguroGMF,
    updateSeguroGMF
} from "../controllers/seguroGMF.controller"

const router = Router();

router.post("/seguroGMF/get", getSeguroGMF)
router.post("/seguroGMF/create", createSeguroGMF)
router.patch("/seguroGMF/update", updateSeguroGMF)

export default router;
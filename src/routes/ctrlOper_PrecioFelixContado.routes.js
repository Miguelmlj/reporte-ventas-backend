import { Router } from "express";

import {
    getPrecioFelixContado,
    createPrecioFelixContado,
    updatePrecioFelixContado
} from "../controllers/ctrlOper_PrecioFelixContado.controller"

const router = Router();

router.post("/ctrloperPrecioFelixContado/get", getPrecioFelixContado);
router.post("/ctrloperPrecioFelixContado/create", createPrecioFelixContado);
router.patch("/ctrloperPrecioFelixContado/update", updatePrecioFelixContado);

export default router;
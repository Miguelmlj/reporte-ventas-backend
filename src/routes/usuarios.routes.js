import { Router } from "express";
import {
    getUsuarios,
    createUsuarios,
    getUsuariosByNombre,
    ExistsUsersDMS,
    CreateUsersDMS,
    DeleteUsersDMS,
    getUserRols,
    InsertNewUsers,
    getUsuarioById,
    updateUsuario
} from "../controllers/usuarios.controller";

const router = Router();

router.get("/usuarios", getUsuarios);

router.get("/usuariosbyid/:id", getUsuarioById);

router.post("/usuarios", createUsuarios);

router.post("/usuariosbynombre", getUsuariosByNombre);

router.patch("/usuarios/update", updateUsuario);

/* 
    1.- crear
    2.- actualizar - this not
    3.- eliminar
    4.- verExistencia
*/
router.get("/usuarios/ExistsUsersDMS", ExistsUsersDMS);
router.get("/usuarios/CreateUsersDMS", CreateUsersDMS);
router.post("/usuarios/InsertNewUsers", InsertNewUsers);
router.delete("/usuarios/DeleteUsersDMS", DeleteUsersDMS);
router.get("/usuarios/UserRols", getUserRols);
// router.post("UpdateUsersDMS", UpdateUsersDMS);

export default router;
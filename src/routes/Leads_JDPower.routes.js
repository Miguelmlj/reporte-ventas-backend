import { Router } from "express";
import {
    getLeadsJDPower,
    createLeadsJDPower,
    updateLeadsJDPower
} from "../controllers/Leads_JDPower.controller";

const router = Router();

router.post("/indicadorJDPower/get", getLeadsJDPower);
router.post("/indicadorJDPower/create", createLeadsJDPower);
router.patch("/indicadorJDPower/update", updateLeadsJDPower);

export default router;
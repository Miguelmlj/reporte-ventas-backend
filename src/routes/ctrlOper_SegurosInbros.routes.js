import { Router } from "express";

import {
    getSegurosInbros,
    createSegurosInbros,
    updateSegurosInbros
} from "../controllers/ctrlOper_SegurosInbros.controller"

const router = Router();

router.post("/ctrloperSegurosInbros/get", getSegurosInbros);
router.post("/ctrloperSegurosInbros/create", createSegurosInbros);
router.patch("/ctrloperSegurosInbros/update", updateSegurosInbros);

export default router;
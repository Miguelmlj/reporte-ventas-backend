export const querys = {
  getNombreAgencia: "SELECT Nombre FROM [Indicadores].[dbo].[Empresas] WHERE Empresa=@empresa AND Sucursal=@sucursal",

  getReporteDiario: "SELECT TOP(500) * FROM [Indicadores].[dbo].[reportes_diarios] ORDER BY id_reporte DESC",

  getReporteDiarioByFecha: `SELECT * FROM [Indicadores].[dbo].[reportes_diarios] WHERE id_fecha = @id_fecha AND Empresa = @empresa AND Sucursal= @sucursal`,

  getAcumuladoDelMes: `SELECT * FROM [Indicadores].[dbo].[reportes_diarios] WHERE MONTH(id_fecha) = @month AND YEAR(id_fecha) = @year AND Empresa = @empresa AND Sucursal = @sucursal`,

  addReporteDiarioTodos:
    `INSERT INTO [Indicadores].[dbo].[reportes_diarios] (
          
          id_fecha, 
          af_fresh_n, 
          af_bback_n, 
          af_primera_cita_n, 
          af_digitales_n, 
          sol_gmf_n, 
          sol_bancos_n, 
          sol_aprobaciones_gmf_n, 
          sol_contratos_comprados_n, 
          af_fresh_s, 
          af_bback_s, 
          af_primera_cita_s, 
          af_digitales_s, 
          sol_gmf_s, 
          sol_bancos_s, 
          sol_aprobaciones_gmf_s, 
          sol_contratos_comprados_s, 
          cit_agendadas_n, 
          cit_cumplidas_n, 
          cit_primera_cita_n, 
          cit_digitales_n
          ) VALUES (
           
            @id_fecha,
            @af_fresh_n, 
            @af_bback_n, 
            @af_primera_cita_n, 
            @af_digitales_n, 
            @sol_gmf_n, 
            @sol_bancos_n, 
            @sol_aprobaciones_gmf_n, 
            @sol_contratos_comprados_n, 
            @af_fresh_s, 
            @af_bback_s, 
            @af_primera_cita_s, 
            @af_digitales_s, 
            @sol_gmf_s, 
            @sol_bancos_s, 
            @sol_aprobaciones_gmf_s, 
            @sol_contratos_comprados_s, 
            @cit_agendadas_n, 
            @cit_cumplidas_n, 
            @cit_primera_cita_n, 
            @cit_digitales_n
              );`,


  // getAllUsuarios: "SELECT * FROM [Indicadores].[dbo].[usuarios_pruebas] ORDER BY Empresa ASC",
  getAllUsuarios: "SELECT * FROM [Indicadores].[dbo].[usuarios] ORDER BY Empresa, nombre_usuario",

  addUsuario: `INSERT INTO [Indicadores].[dbo].[usuarios] (
        Empresa,
        Sucursal,
        nombre_usuario,
        clave,
        tipo_usuario,
        responsable

        ) VALUES (
          @empresa,
          @sucursal,
          @nombre_usuario,
          @clave,
          @tipo_usuario,
          @responsable
          
          );`,

  updatePermissionsUser: `UPDATE [Indicadores].[dbo].[usuarios] 
  SET 
  
  tipo_usuario = @tipo_usuario,
  responsable = @responsable,
  inicio = @inicio,
  afluencia = @afluencia,
  citas = @citas,
  solicitudes = @solicitudes,
  anticipos = @anticipos,
  invfisico = @invfisico,
  codigosQr = @codigosQr,
  usuarios = @usuarios,
  indFYI = @indFYI,
  indLeads = @indLeads,
  indCentroProp = @indCentroProp,
  indJDPower = @indJDPower,
  indBDC = @indBDC,
  indMkt = @indMkt,
  CO_SegGMF = @CO_SegGMF,
  CO_GarGMPlus = @CO_GarGMPlus,
  CO_OnStrt = @CO_OnStrt,
  CO_GAP = @CO_GAP,
  CO_Accsrios = @CO_Accsrios,
  CO_PrecioFlx = @CO_PrecioFlx,
  CO_GarPlus = @CO_GarPlus,
  CO_SegInbros = @CO_SegInbros,
  CO_PrecioFlxCntdo = @CO_PrecioFlxCntdo,
  DtllsCntrtsGMF = @DtllsCntrtsGMF,
  CO_Resumen = @CO_Resumen,
  Asig_Contratos = @Asig_Contratos,
  Clientes_Flotillas = @Clientes_Flotillas,
  OrdenDeCompra_F = @OrdenDeCompra_F

   WHERE 
   id_usuario = @id_usuario`,

  getUserById: `SELECT * FROM [Indicadores].[dbo].[usuarios_pruebas] WHERE id_usuario = @id_usuario`,

  getUsuarioByNombre: `SELECT nombre_usuario FROM [Indicadores].[dbo].[usuarios] WHERE nombre_usuario = @nombre_usuario`,

  //autenticación // login
  login: `SELECT * FROM [Indicadores].[dbo].[usuarios] 
  WHERE nombre_usuario = @nombre AND Empresa = @Empresa AND Sucursal = @Sucursal`,

  loginDMS: `SELECT Usuario, Nombre, Clave FROM [Indicadores].[dbo].[UsuariosDMS] 
    WHERE Usuario = @nombre AND Empresa = @Empresa AND Sucursal = @Sucursal`,

  //----------------------------------------------------------------------------  

  //create reporte diario (CAMPOS DE CITA) los demás campos pasarán a ser nulos
  createCitas: `INSERT INTO [Indicadores].[dbo].[reportes_diarios] (
          Empresa,
          Sucursal,
          id_fecha,
          cit_agendadas_n, 
          cit_cumplidas_n

          ) VALUES (

            @empresa,
            @sucursal,
            @id_fecha,
            @cit_agendadas_n, 
            @cit_cumplidas_n
            );
          `,



  //obtener Registro de citas
  getAllCitas: `SELECT id_reporte, id_fecha, cit_agendadas_n, cit_cumplidas_n FROM [Indicadores].[dbo].[reportes_diarios] WHERE Empresa=@empresa AND Sucursal=@sucursal ORDER BY id_fecha DESC`,
  // getAllCitas: `SELECT id_reporte, id_fecha, cit_agendadas_n, cit_cumplidas_n FROM [Indicadores].[dbo].[reportes_diarios_pruebas] WHERE Empresa=@empresa AND Sucursal=@sucursal ORDER BY id_fecha DESC`,



  //actualizar registro de citas por fecha del día
  updateCitasByFecha: `UPDATE [Indicadores].[dbo].[reportes_diarios] 
        SET 
        Cit_agendadas_n = @cit_agendadas_n, 
        Cit_cumplidas_n = @cit_cumplidas_n
         WHERE 
         Id_fecha = @id_fecha AND Empresa=@empresa AND Sucursal=@sucursal`,



  getCitasByFecha: `SELECT Cit_agendadas_n, Cit_cumplidas_n FROM [Indicadores].[dbo].[reportes_diarios] Where id_fecha = @id_fecha AND Empresa=@empresa AND Sucursal=@sucursal`,
  // getCitasByFecha: `SELECT Cit_agendadas_n, Cit_cumplidas_n FROM [Indicadores].[dbo].[reportes_diarios_pruebas] Where id_fecha = @id_fecha AND Empresa=@empresa AND Sucursal=@sucursal`,


  //----------------------------------------------------------------------------

  //obtener Registro de solicitudes
  getAllSolicitudes: `SELECT id_reporte, id_fecha, sol_gmf_n, sol_bancos_n, sol_aprobaciones_gmf_n, sol_contratos_comprados_n, sol_contratos_comprados_leasing, sol_gmf_s, sol_bancos_s, sol_aprobaciones_gmf_s, sol_contratos_comprados_s FROM [Indicadores].[dbo].[reportes_diarios]  WHERE Empresa=@empresa AND Sucursal=@sucursal ORDER BY id_fecha DESC`,
  // getAllSolicitudes: `SELECT id_reporte, id_fecha, sol_gmf_n, sol_bancos_n, sol_aprobaciones_gmf_n, sol_contratos_comprados_n, sol_contratos_comprados_leasing, sol_gmf_s, sol_bancos_s, sol_aprobaciones_gmf_s, sol_contratos_comprados_s FROM [Indicadores].[dbo].[reportes_diarios_pruebas]  WHERE Empresa=@empresa AND Sucursal=@sucursal ORDER BY id_fecha DESC`,



  getSolicitudesByFecha: `SELECT sol_gmf_n, sol_bancos_n, sol_aprobaciones_gmf_n, sol_contratos_comprados_n, sol_contratos_comprados_leasing, sol_gmf_s, sol_bancos_s, sol_aprobaciones_gmf_s, sol_contratos_comprados_s FROM [Indicadores].[dbo].[reportes_diarios] Where id_fecha = @id_fecha AND Empresa=@empresa AND Sucursal=@sucursal`,
  // getSolicitudesByFecha: `SELECT sol_gmf_n, sol_bancos_n, sol_aprobaciones_gmf_n, sol_contratos_comprados_n, sol_contratos_comprados_leasing, sol_gmf_s, sol_bancos_s, sol_aprobaciones_gmf_s, sol_contratos_comprados_s FROM [Indicadores].[dbo].[reportes_diarios_pruebas] Where id_fecha = @id_fecha AND Empresa=@empresa AND Sucursal=@sucursal`,



  // createSolicitudes: `INSERT INTO [Indicadores].[dbo].[reportes_diarios_pruebas] (
  createSolicitudes: `INSERT INTO [Indicadores].[dbo].[reportes_diarios] (
    Empresa,
    Sucursal,
    id_fecha, 
    sol_gmf_n, 
    sol_bancos_n, 
    sol_aprobaciones_gmf_n, 
    sol_contratos_comprados_n,
    sol_contratos_comprados_leasing,

    sol_gmf_s, 
    sol_bancos_s, 
    sol_aprobaciones_gmf_s, 
    sol_contratos_comprados_s

    ) VALUES (
      @empresa,
      @sucursal,
      @id_fecha,
      @sol_gmf_n, 
      @sol_bancos_n, 
      @sol_aprobaciones_gmf_n, 
      @sol_contratos_comprados_n,
      @sol_contratos_comprados_leasing,

      @sol_gmf_s, 
      @sol_bancos_s, 
      @sol_aprobaciones_gmf_s, 
      @sol_contratos_comprados_s
      );`,



      // updateSolicitudesByFecha: `UPDATE [Indicadores].[dbo].[reportes_diarios_pruebas] 
     updateSolicitudesByFecha: `UPDATE [Indicadores].[dbo].[reportes_diarios] 
     SET 
     sol_gmf_n = @sol_gmf_n, 
     sol_bancos_n = @sol_bancos_n, 
     sol_aprobaciones_gmf_n = @sol_aprobaciones_gmf_n, 
     sol_contratos_comprados_n = @sol_contratos_comprados_n,
     sol_contratos_comprados_leasing = @sol_contratos_comprados_leasing,

     sol_gmf_s = @sol_gmf_s,
     sol_bancos_s = @sol_bancos_s,
     sol_aprobaciones_gmf_s = @sol_aprobaciones_gmf_s,
     sol_contratos_comprados_s = @sol_contratos_comprados_s
      WHERE 
      Id_fecha = @id_fecha AND Empresa=@empresa AND Sucursal=@sucursal`,


  //----------------------------------------------------------------------------

  //obtener Registro de afluencia
  getAllAfluencia: `SELECT id_reporte, id_fecha, af_fresh_n, af_bback_n, af_primera_cita_n, af_digitales_n, af_fresh_s, af_bback_s, af_primera_cita_s, af_digitales_s FROM [Indicadores].[dbo].[reportes_diarios] WHERE Empresa=@empresa AND Sucursal=@sucursal ORDER BY id_fecha DESC`,
  // getAllAfluencia: `SELECT id_reporte, id_fecha, af_fresh_n, af_bback_n, af_primera_cita_n, af_digitales_n, af_fresh_s, af_bback_s, af_primera_cita_s, af_digitales_s FROM [Indicadores].[dbo].[reportes_diarios_pruebas] WHERE Empresa=@empresa AND Sucursal=@sucursal ORDER BY id_fecha DESC`,




  //general, para el los administraddores
  getAfluenciaByFecha: `SELECT af_fresh_n, af_bback_n, af_primera_cita_n, af_digitales_n, af_fresh_s, af_bback_s, af_primera_cita_s, af_digitales_s FROM [Indicadores].[dbo].[reportes_diarios] Where id_fecha = @id_fecha AND Empresa=@empresa AND Sucursal=@sucursal`,
  // getAfluenciaByFecha: `SELECT af_fresh_n, af_bback_n, af_primera_cita_n, af_digitales_n, af_fresh_s, af_bback_s, af_primera_cita_s, af_digitales_s FROM [Indicadores].[dbo].[reportes_diarios_pruebas] Where id_fecha = @id_fecha AND Empresa=@empresa AND Sucursal=@sucursal`,

  // afluencia para los nuevos autos

  getAfluenciaByFechaNuevos: `SELECT af_fresh_n, af_bback_n, af_primera_cita_n, af_digitales_n FROM [Indicadores].[dbo].[reportes_diarios] Where id_fecha = @id_fecha AND Empresa=@empresa AND Sucursal=@sucursal`,
  // getAfluenciaByFechaNuevos: `SELECT af_fresh_n, af_bback_n, af_primera_cita_n, af_digitales_n FROM [Indicadores].[dbo].[reportes_diarios_pruebas] Where id_fecha = @id_fecha AND Empresa=@empresa AND Sucursal=@sucursal`,

  // afluencia para los seminuevos autos

  getAfluenciaByFechaSeminuevos: `SELECT af_fresh_s, af_bback_s, af_primera_cita_s, af_digitales_s FROM [Indicadores].[dbo].[reportes_diarios] Where id_fecha = @id_fecha AND Empresa=@empresa AND Sucursal=@sucursal`,
  // getAfluenciaByFechaSeminuevos: `SELECT af_fresh_s, af_bback_s, af_primera_cita_s, af_digitales_s FROM [Indicadores].[dbo].[reportes_diarios_pruebas] Where id_fecha = @id_fecha AND Empresa=@empresa AND Sucursal=@sucursal`,


  //para administrador
  createAfluencia: `INSERT INTO [Indicadores].[dbo].[reportes_diarios] (
          Empresa,
          Sucursal,
          id_fecha, 
          af_fresh_n, 
          af_bback_n, 
          af_primera_cita_n, 
          af_digitales_n,

          af_fresh_s, 
          af_bback_s, 
          af_primera_cita_s, 
          af_digitales_s

          ) VALUES (
            @empresa,
            @sucursal,
            @id_fecha,
            @af_fresh_n, 
            @af_bback_n, 
            @af_primera_cita_n, 
            @af_digitales_n,

            @af_fresh_s, 
            @af_bback_s, 
            @af_primera_cita_s, 
            @af_digitales_s
            );
          `,

  //para hostess nuevos
  createAfluenciaNuevos: `INSERT INTO [Indicadores].[dbo].[reportes_diarios] (
          Empresa,
          Sucursal,
          id_fecha, 
          af_fresh_n, 
          af_bback_n, 
          af_primera_cita_n, 
          af_digitales_n

          ) VALUES (
            @empresa,
            @sucursal,
            @id_fecha,
            @af_fresh_n, 
            @af_bback_n, 
            @af_primera_cita_n, 
            @af_digitales_n
            
            );
          `,

  //para hostess seminuevos
  createAfluenciaSeminuevos: `INSERT INTO [Indicadores].[dbo].[reportes_diarios] (
          Empresa,
          Sucursal,
          id_fecha,
          af_fresh_s, 
          af_bback_s, 
          af_primera_cita_s, 
          af_digitales_s

          ) VALUES (
            @empresa,
            @sucursal,
            @id_fecha,
            @af_fresh_s, 
            @af_bback_s, 
            @af_primera_cita_s, 
            @af_digitales_s
            );
          `,



  //Gestiona todas las agencias
  //modo admin
  updateAfluenciaByFecha: `UPDATE [Indicadores].[dbo].[reportes_diarios] 
          SET 
          af_fresh_n = @af_fresh_n, 
          af_bback_n = @af_bback_n, 
          af_primera_cita_n = @af_primera_cita_n, 
          af_digitales_n = @af_digitales_n,

          af_fresh_s = @af_fresh_s,
          af_bback_s = @af_bback_s,
          af_primera_cita_s = @af_primera_cita_s,
          af_digitales_s = @af_digitales_s
           WHERE 
           Id_fecha = @id_fecha AND Empresa=@empresa AND Sucursal=@sucursal`,

  //actualiza campos afluencia nuevos
  updateAfluenciaByFechaNuevos: `UPDATE [Indicadores].[dbo].[reportes_diarios] 
          SET 
          af_fresh_n = @af_fresh_n, 
          af_bback_n = @af_bback_n, 
          af_primera_cita_n = @af_primera_cita_n, 
          af_digitales_n = @af_digitales_n
         
           WHERE 
           Id_fecha = @id_fecha AND Empresa=@empresa AND Sucursal=@sucursal`,

  //actualiza campos afluencia seminuevos
  updateAfluenciaByFechaSeminuevos: `UPDATE [Indicadores].[dbo].[reportes_diarios] 
          SET 
          
          af_fresh_s = @af_fresh_s,
          af_bback_s = @af_bback_s,
          af_primera_cita_s = @af_primera_cita_s,
          af_digitales_s = @af_digitales_s
           WHERE 
           Id_fecha = @id_fecha AND Empresa=@empresa AND Sucursal=@sucursal`,


  //  QURIES PARA MODULO ANTICIPOS
  createAnticipo: `INSERT INTO [Indicadores].[dbo].[Anticipos] (
    Empresa,
    Sucursal,
    Folio_anticipo,
    Fecha,
    
    Nombre_cliente,
    Unidad,
    Paquete,
    Color,
    Tipo_compra,
    Nombre_asesor,
    Agencia

  ) VALUES (
    @Empresa,
    @Sucursal,
    @Folio_anticipo,
    @Fecha,
    
    @Nombre_cliente,
    @Unidad,
    @Paquete,
    @Color,
    @Tipo_compra,
    @Nombre_asesor,
    @Agencia

  );`,

  //solo usuarios TI
  // getAllAnticipos:`SELECT * FROM [Indicadores].[dbo].[Anticipos] WHERE Empresa=@Empresa AND Sucursal=@Sucursal ORDER BY Fecha DESC`,
  getAllAnticipos: `SELECT * FROM [Indicadores].[dbo].[Anticipos] ORDER BY Fecha DESC`,
  getAnticiposGerente: `SELECT * FROM [Indicadores].[dbo].[Anticipos] WHERE Empresa=@Empresa AND Sucursal=@Sucursal ORDER BY Fecha DESC`,
  getAnticiposOfAsesor: `SELECT * FROM [Indicadores].[dbo].[Anticipos] WHERE Empresa=@Empresa AND Sucursal=@Sucursal AND Nombre_asesor=@Nombre_asesor ORDER BY Fecha DESC`,

  getAnticipoById: `SELECT * FROM [Indicadores].[dbo].[Anticipos] WHERE Id_anticipo=@Id_anticipo`,

  updateAnticipo: `UPDATE [Indicadores].[dbo].[Anticipos] 
    SET 
    Empresa = @Empresa,
    Sucursal = @Sucursal,
    Folio_anticipo = @Folio_anticipo,
    Fecha = @Fecha,
    Nombre_cliente = @Nombre_cliente,
    Unidad = @Unidad,
    Paquete = @Paquete,
    Color = @Color,
    Tipo_compra = @Tipo_compra,
    Nombre_asesor = @Nombre_asesor,
    Agencia = @Agencia
    WHERE
    Id_anticipo = @Id_anticipo`,

  getInventario: `SELECT * FROM [Indicadores].[dbo].[Inventario] WHERE Id_fecha = @Id_fecha AND Empresa=@Empresa AND Sucursal=@Sucursal AND Auditor=@Auditor`,

  inventarioExisteEnHistorial: `SELECT * FROM [Indicadores].[dbo].[InvHist] WHERE Empresa=@Empresa AND Sucursal=@Sucursal AND Id_fecha=@Id_fecha AND Auditor=@Auditor`,

  getInventarioSistema: `SELECT * FROM [Indicadores].[dbo].[VehiculosDMS] WHERE Empresa=@Empresa AND Sucursal=@Sucursal`,
  getVehiculosDMS: `SELECT * FROM [Indicadores].[dbo].[VehiculosDMS]`,

  getInventarioSistemaQr: `SELECT Empresa, Sucursal, vehi_serie,
    impreso = isnull((select isnull( impreso, 'N') 
    FROM Vehiculos as v2 
    where v2.Empresa=v.Empresa AND v2.Sucursal = v.Sucursal 
    AND 
    v2.vehi_serie = v.vehi_serie),'N') 
    FROM [Indicadores].[dbo].[VehiculosDMS] 
    as v WHERE v.Empresa=@Empresa AND v.Sucursal=@Sucursal ORDER BY impreso`,

  // getInventarioSistemaQr: `SELECT vehi_serie FROM [Indicadores].[dbo].[VehiculosDMS] WHERE Empresa=@Empresa AND Sucursal=@Sucursal`,
  // getInventarioHistorial: `SELECT * FROM [Indicadores].[dbo].[InvHist] WHERE Id_fecha = @Id_fecha AND Empresa=@Empresa AND Sucursal=@Sucursal`,
  getInventarioHistorial: `SELECT Id_vehiculo, Anio_vehi, Marca, Modelo, Serie_sistema, Serie_fisico, Ubicacion, QRCapturado, UbicacionDMS FROM [Indicadores].[dbo].[InvHist] WHERE Id_fecha = @Id_fecha AND Empresa=@Empresa AND Sucursal=@Sucursal AND Auditor=@Auditor ORDER BY Modelo`,
  eliminarInventarioEnHistorial: `DELETE FROM [Indicadores].[dbo].[InvHist] WHERE Empresa=@Empresa AND Sucursal=@Sucursal AND Id_fecha=@Id_fecha AND Auditor=@Auditor`,
  getInventarioZap_Aero: `SELECT * FROM [Indicadores].[dbo].[Inventario] WHERE Id_fecha = @Id_fecha AND Empresa=5 AND Sucursal>= 1 AND  Sucursal<=2  AND Auditor=@Auditor`,
  getFechasInventario: `SELECT DISTINCT Id_fecha FROM [Indicadores].[dbo].[Inventario] WHERE Empresa=@Empresa AND Sucursal=@Sucursal AND Auditor=@Auditor ORDER BY Id_fecha DESC`,
  getFechasInventarioCuliacan: `SELECT DISTINCT Id_fecha FROM [Indicadores].[dbo].[Inventario] WHERE Empresa=5 AND Sucursal>= 1 AND  Sucursal<=2 AND Auditor=@Auditor ORDER BY Id_fecha DESC`,

  insertVehi_DMS: `INSERT INTO [Indicadores].[dbo].[Vehiculos] (
      Empresa,
      Sucursal,
      Inventario,
      mode_clave,
      mode_tipo,
      mode_descripcion,
      vehi_serie,
      color,
      marc_descrip,
      vehi_anio_modelo,
      Vehi_clase,
      impreso

      ) VALUES (
        @Empresa,
        @Sucursal,
        @Inventario,
        @mode_clave,
        @mode_tipo,
        @mode_descripcion,
        @vehi_serie,
        @color,
        @marc_descrip,
        @vehi_anio_modelo,
        @Vehi_clase,
        @impreso

        );`,

  crearInventarioEnHistorial: `INSERT INTO [Indicadores].[dbo].[InvHist] (
      Empresa,
      Sucursal,
      Id_fecha,
      Id_vehiculo,
      Anio_vehi,
      Marca,
      Modelo,
      Serie_sistema,
      Serie_fisico,
      Ubicacion,
      Auditor,
      QRCapturado,
      UbicacionDMS

      ) VALUES (
        @Empresa,
        @Sucursal,
        @Id_fecha,
        @Id_vehiculo,
        @Anio_vehi,
        @Marca,
        @Modelo,
        @Serie_sistema,
        @Serie_fisico,
        @Ubicacion,
        @Auditor,
        @QRCapturado,
        @UbicacionDMS

        );`,

  //se buscará solo un campo en la base de datos, es solo para saber si hay registros, no es necesario obtener todos los campos.
  // ExistsUsersDMS: `SELECT Id_usuario FROM [Indicadores].[dbo].[UsuariosDMS_pruebas]`,
  ExistsUsersDMS: `SELECT Id_usuario FROM [Indicadores].[dbo].[UsuariosDMS]`,

  /*  CreateUsersDMS: `INSERT INTO [Indicadores].[dbo].[UsuariosDMS_pruebas] (
     Empresa,
     Sucursal,
     Usuario,
     Nombre,
     Clave,
     Puesto,
     DMS
 
     ) VALUES (
       @Empresa,
       @Sucursal,
       @Usuario,
       @Nombre,
       @Clave,
       @Puesto,
       @DMS
      
       );`, */

  CreateUsersDMS: `INSERT INTO [Indicadores].[dbo].[UsuariosDMS] (
        Empresa,
        Sucursal,
        Usuario,
        Nombre,
        Clave,
        Puesto,
        DMS
  
        ) VALUES (
          @Empresa,
          @Sucursal,
          @Usuario,
          @Nombre,
          @Clave,
          @Puesto,
          @DMS
         
          );`,

  // DeleteUsersDMS:`DELETE FROM [Indicadores].[dbo].[UsuariosDMS_pruebas] WHERE DMS=1`,
  DeleteUsersDMS: `DELETE FROM [Indicadores].[dbo].[UsuariosDMS] WHERE DMS=1`,

  // ResetIdUsuarioIncrementalUsersDMS:`DBCC CHECKIDENT ('[Indicadores].[dbo].[UsuariosDMS]', RESEED, 0)`,
  ResetIdUsuarioIncrementalUsersDMS: `DBCC CHECKIDENT ('[Indicadores].[dbo].[UsuariosDMS]', RESEED, 37)`,

  getUserRols: `SELECT * FROM [Indicadores].[dbo].[responsable_usuarios]`,

  // ExistsUserOnTable: `SELECT nombre_usuario FROM [Indicadores].[dbo].[usuarios_pruebas] where nombre_usuario = @nombre AND Empresa = @Empresa AND Sucursal = @Sucursal`,
  ExistsUserOnTable: `SELECT nombre_usuario FROM [Indicadores].[dbo].[usuarios] where nombre_usuario = @nombre AND Empresa = @Empresa AND Sucursal = @Sucursal`,

  /* InsertUserFromUsuariosDMSToUsuarios: `INSERT INTO [Indicadores].[dbo].[usuarios_pruebas] (
    Empresa,
    Sucursal,
    nombre_usuario,
    clave,
    tipo_usuario,
    responsable,
    inicio,
    afluencia,
    citas,
    solicitudes,
    anticipos,
    invfisico,
    codigosQr,
    usuarios
 
    ) VALUES (
      @empresa,
      @sucursal,
      @nombre_usuario,
      @clave,
      @tipo_usuario,
      @responsable,
      @inicio,
    @afluencia,
    @citas,
    @solicitudes,
    @anticipos,
    @invfisico,
    @codigosQr,
    @usuarios
      );`, */

  InsertUserFromUsuariosDMSToUsuarios: `INSERT INTO [Indicadores].[dbo].[usuarios] (
            Empresa,
            Sucursal,
            nombre_usuario,
            clave,
            tipo_usuario,
            responsable,
            inicio,
            afluencia,
            citas,
            solicitudes,
            anticipos,
            invfisico,
            codigosQr,
            usuarios,
            indFYI,
            indLeads,
            indCentroProp,
            indJDPower,
            indBDC,
            indMkt,
            CO_SegGMF,
            CO_GarGMPlus,
            CO_OnStrt,
            CO_GAP,
            CO_Accsrios,
            CO_PrecioFlx,
            CO_GarPlus,
            CO_SegInbros,
            CO_PrecioFlxCntdo,
            DtllsCntrtsGMF,
            CO_Resumen
    
            ) VALUES (
              @empresa,
              @sucursal,
              @nombre_usuario,
              @clave,
              @tipo_usuario,
              @responsable,
              @inicio,
            @afluencia,
            @citas,
            @solicitudes,
            @anticipos,
            @invfisico,
            @codigosQr,
            @usuarios,
            @indFYI,
            @indLeads,
            @indCentroProp,
            @indJDPower,
            @indBDC,
            @indMkt,
            @CO_SegGMF,
            @CO_GarGMPlus,
            @CO_OnStrt,
            @CO_GAP,
            @CO_Accsrios,
            @CO_PrecioFlx,
            @CO_GarPlus,
            @CO_SegInbros,
            @CO_PrecioFlxCntdo,
            @DtllsCntrtsGMF,
            @CO_Resumen
              );`,

  //getFechasInventarioCuliacan: `SELECT * FROM [Indicadores].[dbo].[Inventario] WHERE Id_fecha = '2022-05-03' AND Empresa=5 AND Sucursal>= 1 AND  Sucursal<=2`


  /* REQUEST SQL TO QR CODES */
  ExistsVehiculosSQL: `SELECT vehi_serie FROM [Indicadores].[dbo].[Vehiculos]`,
  getVehiculosSQL: `SELECT * FROM [Indicadores].[dbo].[Vehiculos]`,
  deleteVehiculosSQL: `DELETE FROM [Indicadores].[dbo].[Vehiculos] WHERE vehi_serie = @vehi_serie`,

  VINExistsInTable: `
  SELECT vehi_serie 
  FROM [Indicadores].[dbo].[Vehiculos]
  WHERE 
  vehi_serie = @vehi_serie AND Empresa = @Empresa AND Sucursal = @Sucursal
  `,
  InsertVINToVehiculos: `INSERT INTO [Indicadores].[dbo].[Vehiculos] (
    Empresa,
    Sucursal,
    vehi_serie,
    impreso,
    Fecha
    
    ) VALUES (
      @empresa,
      @sucursal,
      @vehi_serie,
      @impreso,
      @Fecha
      
      );`,


  /* INDICADORES */
  /* QUERIES INDICADORES FYI */

  createFYI_indicador: `INSERT INTO [Indicadores].[dbo].[FYI_Indicadores] (
        Empresa,
        Sucursal,
        Fecha,
        Garantias_extendidas,
        Seguros,
        On_start,
        GAP,
        Importe_accesorios
        
        ) VALUES (
          @Empresa,
          @Sucursal,
          @Fecha,
          @GarExtendidas,
          @Seguros,
          @OnStart,
          @ProgValorFactura,
          @ImporteAccesorios
          
          );`,

  getFYI_indicador: `SELECT 
          Id_indicador,
          Fecha,
          Garantias_extendidas,
          Seguros,
          On_start,
          GAP,
          Importe_accesorios
          FROM 
          [Indicadores].[dbo].[FYI_Indicadores] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal ORDER BY Fecha DESC`,

  updateFYI_indicador: `UPDATE [Indicadores].[dbo].[FYI_Indicadores] 
    SET
    Fecha = @Fecha,
    Garantias_extendidas = @GarExtendidas,
    Seguros = @Seguros,
    On_start = @OnStart,
    GAP = @ProgValorFactura,
    Importe_accesorios = @ImporteAccesorios
    
    WHERE
    Id_indicador = @Id_indicador`,


  /* QUERIES INDICADORES LEADS CONTACTACIÓN */
  createLeads_indicador: `INSERT INTO [Indicadores].[dbo].[Leads_Indicadores] (
      Empresa,
      Sucursal,
      Fecha,
      Oport_registradasPeriodo,
      Oport_contactadas,
      Oport_contactadasATiempo,
      Porcen_contactadosATiempo
      
      ) VALUES (
        @Empresa,
        @Sucursal,
        @Fecha,
        @Oport_registradasPeriodo,
        @Oport_contactadas,
        @Oport_contactadasATiempo,
        @Porcen_contactadosATiempo
        
        );`,

  getLeads_indicador: `SELECT 
        Id,
        Fecha,
        Oport_registradasPeriodo,
        Oport_contactadas,
        Oport_contactadasATiempo,
        Porcen_contactadosATiempo 

        FROM 
        [Indicadores].[dbo].[Leads_Indicadores] 
        WHERE
        Empresa = @Empresa
        AND
        Sucursal = @Sucursal ORDER BY Fecha DESC`,

  updateLeads_indicador: `UPDATE [Indicadores].[dbo].[Leads_Indicadores] 
    SET
    Fecha = @Fecha,
    Oport_registradasPeriodo = @Oport_registradasPeriodo,
    Oport_contactadas = @Oport_contactadas,
    Oport_contactadasATiempo = @Oport_contactadasATiempo,
    Porcen_contactadosATiempo = @Porcen_contactadosATiempo
    
    WHERE
    Id = @Id`,

  /* QUERIES INDICADORES CENTRO DE PROPIETARIO */

  getLeadsCentroProp: `SELECT 
        Id,
        Fecha,
        Encuesta_pulso

        FROM 
        [Indicadores].[dbo].[CentroProp_Indicadores] 
        WHERE
        Empresa = @Empresa
        AND
        Sucursal = @Sucursal ORDER BY Fecha DESC`,

  createLeadsCentroProp: `INSERT INTO [Indicadores].[dbo].[CentroProp_Indicadores] (
          Empresa,
          Sucursal,
          Fecha,
          Encuesta_pulso
          
          ) VALUES (
            @Empresa,
            @Sucursal,
            @Fecha,
            @Encuesta_pulso
            
            );`,

  updateLeadsCentroProp: `UPDATE [Indicadores].[dbo].[CentroProp_Indicadores] 
  SET
  Fecha = @Fecha,
  Encuesta_pulso = @Encuesta_pulso
  
  WHERE
  Id = @Id`,

  /* QUERIES INDICADORES JD POWER */

  getLeadsJDPower: `SELECT 
  Id,
  Fecha,
  Cantidad_encuestas

  FROM 
  [Indicadores].[dbo].[JDPower_Indicadores] 
  WHERE
  Empresa = @Empresa
  AND
  Sucursal = @Sucursal ORDER BY Fecha DESC`,

  createLeadsJDPower: `INSERT INTO [Indicadores].[dbo].[JDPower_Indicadores] (
    Empresa,
    Sucursal,
    Fecha,
    Cantidad_encuestas
    
    ) VALUES (
      @Empresa,
      @Sucursal,
      @Fecha,
      @Cantidad_encuestas
      
      );`,

  updateLeadsJDPower: `UPDATE [Indicadores].[dbo].[JDPower_Indicadores] 
  SET
  Fecha = @Fecha,
  Cantidad_encuestas = @Cantidad_encuestas
  
  WHERE
  Id = @Id`,

  /* QUERIES INDICADORES BDC */
  getIndicadorBDC: `SELECT 
  Id,
  Fecha,
  Llamadas_entrantesVehNvo

  FROM 
  [Indicadores].[dbo].[BDC_Indicadores] 
  WHERE
  Empresa = @Empresa
  AND
  Sucursal = @Sucursal ORDER BY Fecha DESC`,

  createIndicadorBDC: `INSERT INTO [Indicadores].[dbo].[BDC_Indicadores] (
    Empresa,
    Sucursal,
    Fecha,
    Llamadas_entrantesVehNvo
    
    ) VALUES (
      @Empresa,
      @Sucursal,
      @Fecha,
      @Llamadas_entrantesVehNvo
      
      );`,

  updateIndicadorBDC: `UPDATE [Indicadores].[dbo].[BDC_Indicadores] 
      SET
      Fecha = @Fecha,
      Llamadas_entrantesVehNvo = @Llamadas_entrantesVehNvo
      
      WHERE
      Id = @Id`,

  /* QUERIES INDICADORES MERCADOTECNIA */
  getIndicadorMkt: `SELECT 
      Id,
      Fecha,
      Lead_GrupoFelix,
      Lead_PaginaLocal
    
      FROM 
      [Indicadores].[dbo].[Mkt_Indicadores] 
      WHERE
      Empresa = @Empresa
      AND
      Sucursal = @Sucursal ORDER BY Fecha DESC`,

  createIndicadorMkt: `INSERT INTO [Indicadores].[dbo].[Mkt_Indicadores] (
        Empresa,
        Sucursal,
        Fecha,
        Lead_GrupoFelix,
        Lead_PaginaLocal
        
        ) VALUES (
          @Empresa,
          @Sucursal,
          @Fecha,
          @Lead_GrupoFelix,
          @Lead_PaginaLocal
          
          );`,

  updateIndicadorMkt: `UPDATE [Indicadores].[dbo].[Mkt_Indicadores] 
          SET
          Fecha = @Fecha,
          Lead_GrupoFelix = @Lead_GrupoFelix,
          Lead_PaginaLocal = @Lead_PaginaLocal
          
          WHERE
          Id = @Id`,

  /* QUERIES SEGUROS GMF*/
  getSeguroGMF: `SELECT 
          Id,
          Fecha,
          Importe_Seguro,
          Asesor,
          Nombre_Cliente,
          Utilidad,
          Comision_Asesor,
          Utilidad_Neta

          FROM 
          [Indicadores].[dbo].[CtrlOper_SeguroGMF] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal ORDER BY Fecha DESC`,


  createSeguroGMF: `INSERT INTO [Indicadores].[dbo].[CtrlOper_SeguroGMF] (
            Empresa,
            Sucursal,
            Fecha,
            Importe_Seguro,
            Asesor,
            Nombre_Cliente,
            Utilidad,
            Comision_Asesor,
            Utilidad_Neta
            
            ) VALUES (
              @Empresa,
              @Sucursal,
              @Fecha,
              @Importe_Seguro,
              @Asesor,
              @Nombre_Cliente,
              @Utilidad,
              @Comision_Asesor,
              @Utilidad_Neta
              
              );`,

  updateSeguroGMF: `UPDATE [Indicadores].[dbo].[CtrlOper_SeguroGMF] 
              SET
              Fecha = @Fecha,
              Importe_Seguro = @Importe_Seguro,
              Asesor = @Asesor,
              Nombre_Cliente = @Nombre_Cliente,
              Utilidad = @Utilidad,
              Comision_Asesor = @Comision_Asesor,
              Utilidad_Neta = @Utilidad_Neta
              
              WHERE
              Id = @Id`,

  /* QUERIES GARANTIAS GM PLUS*/
  getGarantiasGMPlus: `SELECT 
          Id,
          Fecha,
          Importe_Garantia,
          Asesor,
          Nombre_Cliente,
          Utilidad,
          Comision_Asesor,
          Utilidad_Neta

          FROM 
          [Indicadores].[dbo].[CtrlOper_GarantiasGMPlus] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal ORDER BY Fecha DESC`,


  createGarantiasGMPlus: `INSERT INTO [Indicadores].[dbo].[CtrlOper_GarantiasGMPlus] (
            Empresa,
            Sucursal,
            Fecha,
            Importe_Garantia,
            Asesor,
            Nombre_Cliente,
            Utilidad,
            Comision_Asesor,
            Utilidad_Neta
            
            ) VALUES (
              @Empresa,
              @Sucursal,
              @Fecha,
              @Importe_Garantia,
              @Asesor,
              @Nombre_Cliente,
              @Utilidad,
              @Comision_Asesor,
              @Utilidad_Neta
              
              );`,

  updateGarantiasGMPlus: `UPDATE [Indicadores].[dbo].[CtrlOper_GarantiasGMPlus] 
              SET
              Fecha = @Fecha,
              Importe_Garantia = @Importe_Garantia,
              Asesor = @Asesor,
              Nombre_Cliente = @Nombre_Cliente,
              Utilidad = @Utilidad,
              Comision_Asesor = @Comision_Asesor,
              Utilidad_Neta = @Utilidad_Neta
              
              WHERE
              Id = @Id`,

  /* QUERIES Ctrl Operativo Onstart*/
  getCtrlOper_OnStar: `SELECT 
          Id,
          Fecha,
          Importe_OnStar,
          Asesor,
          Nombre_Cliente,
          Utilidad,
          Comision_Asesor,
          Utilidad_Neta

          FROM 
          [Indicadores].[dbo].[CtrlOper_OnStar] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal ORDER BY Fecha DESC`,

  ExistsRegister_OnStar: `SELECT * FROM [Indicadores].[dbo].[CtrlOper_OnStar] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal 
          AND
          Fecha = @Fecha`,

  createCtrlOper_OnStar: `INSERT INTO [Indicadores].[dbo].[CtrlOper_OnStar] (
            Empresa,
            Sucursal,
            Fecha,
            Importe_OnStar,
            Asesor,
            Nombre_Cliente,
            Utilidad,
            Comision_Asesor,
            Utilidad_Neta
            
            ) VALUES (
              @Empresa,
              @Sucursal,
              @Fecha,
              @Importe_OnStar,
              @Asesor,
              @Nombre_Cliente,
              @Utilidad,
              @Comision_Asesor,
              @Utilidad_Neta
              
              );`,

  updateCtrlOper_OnStar: `UPDATE [Indicadores].[dbo].[CtrlOper_OnStar] 
              SET
              Fecha = @Fecha,
              Importe_OnStar = @Importe_OnStar,
              Asesor = @Asesor,
              Nombre_Cliente = @Nombre_Cliente,
              Utilidad = @Utilidad,
              Comision_Asesor = @Comision_Asesor,
              Utilidad_Neta = @Utilidad_Neta
              
              WHERE
              Id = @Id`,


  /* QUERIES Ctrl Operativo GAP*/
  getCtrlOper_GAP: `SELECT 
          Id,
          Fecha,
          Importe_GAP,
          Asesor,
          Nombre_Cliente,
          Utilidad,
          Comision_Asesor,
          Utilidad_Neta

          FROM 
          [Indicadores].[dbo].[CtrlOper_GAP] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal ORDER BY Fecha DESC`,

  ExistsRegister_GAP: `SELECT * FROM [Indicadores].[dbo].[CtrlOper_GAP] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal 
          AND
          Fecha = @Fecha`,

  createCtrlOper_GAP: `INSERT INTO [Indicadores].[dbo].[CtrlOper_GAP] (
            Empresa,
            Sucursal,
            Fecha,
            Importe_GAP,
            Asesor,
            Nombre_Cliente,
            Utilidad,
            Comision_Asesor,
            Utilidad_Neta
            
            ) VALUES (
              @Empresa,
              @Sucursal,
              @Fecha,
              @Importe_GAP,
              @Asesor,
              @Nombre_Cliente,
              @Utilidad,
              @Comision_Asesor,
              @Utilidad_Neta
              
              );`,

  updateCtrlOper_GAP: `UPDATE [Indicadores].[dbo].[CtrlOper_GAP] 
              SET
              Fecha = @Fecha,
              Importe_GAP = @Importe_GAP,
              Asesor = @Asesor,
              Nombre_Cliente = @Nombre_Cliente,
              Utilidad = @Utilidad,
              Comision_Asesor = @Comision_Asesor,
              Utilidad_Neta = @Utilidad_Neta
              
              WHERE
              Id = @Id`,

  /* QUERIES Ctrl Operativo Accesorios*/
  getCtrlOper_Accesorios: `SELECT 
          Id,
          Fecha,
          Importe_Accesorio,
          Descrip_Accesorio,
          Codigo_Accesorio,
          Costo_Accesorio,
          Asesor,
          Nombre_Cliente,
          Utilidad,
          Comision_Asesor,
          Utilidad_Neta

          FROM 
          [Indicadores].[dbo].[CtrlOper_Accesorios] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal ORDER BY Fecha DESC`,

  ExistsRegister_Accesorios: `SELECT * FROM [Indicadores].[dbo].[CtrlOper_Accesorios] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal 
          AND
          Fecha = @Fecha`,

  createCtrlOper_Accesorios: `INSERT INTO [Indicadores].[dbo].[CtrlOper_Accesorios] (
            Empresa,
            Sucursal,
            Fecha,
            Importe_Accesorio,
            Descrip_Accesorio,
            Codigo_Accesorio,
            Costo_Accesorio,
            Asesor,
            Nombre_Cliente,
            Utilidad,
            Comision_Asesor,
            Utilidad_Neta
            
            ) VALUES (
              @Empresa,
              @Sucursal,
              @Fecha,
              @Importe_Accesorio,
              @Descrip_Accesorio,
              @Codigo_Accesorio,
              @Costo_Accesorio,
              @Asesor,
              @Nombre_Cliente,
              @Utilidad,
              @Comision_Asesor,
              @Utilidad_Neta
              
              );`,

  updateCtrlOper_Accesorios: `UPDATE [Indicadores].[dbo].[CtrlOper_Accesorios] 
              SET
              Fecha = @Fecha,
              Importe_Accesorio = @Importe_Accesorio,
              Descrip_Accesorio = @Descrip_Accesorio,
              Codigo_Accesorio = @Codigo_Accesorio,
              Costo_Accesorio = @Costo_Accesorio,
              Asesor = @Asesor,
              Nombre_Cliente = @Nombre_Cliente,
              Utilidad = @Utilidad,
              Comision_Asesor = @Comision_Asesor,
              Utilidad_Neta = @Utilidad_Neta
              
              WHERE
              Id = @Id`,

  /* QUERIES Ctrl Operativo Precio Felix*/
  getCtrlOper_PrecioFelix: `SELECT 
          Id,
          Fecha,
          Importe_PrecioFelix,
          Asesor,
          Nombre_Cliente,
          Factura_PrecioFelix,
          Utilidad,
          Comision_Asesor,
          Utilidad_Neta

          FROM 
          [Indicadores].[dbo].[CtrlOper_PrecioFelix] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal ORDER BY Fecha DESC`,

  ExistsRegister_PrecioFelix: `SELECT * FROM [Indicadores].[dbo].[CtrlOper_PrecioFelix] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal 
          AND
          Fecha = @Fecha`,

  createCtrlOper_PrecioFelix: `INSERT INTO [Indicadores].[dbo].[CtrlOper_PrecioFelix] (
            Empresa,
            Sucursal,
            Fecha,
            Importe_PrecioFelix,
            Asesor,
            Nombre_Cliente,
            Factura_PrecioFelix,
            Utilidad,
            Comision_Asesor,
            Utilidad_Neta
            
            ) VALUES (
              @Empresa,
              @Sucursal,
              @Fecha,
              @Importe_PrecioFelix,
              @Asesor,
              @Nombre_Cliente,
              @Factura_PrecioFelix,
              @Utilidad,
              @Comision_Asesor,
              @Utilidad_Neta
              
              );`,

  updateCtrlOper_PrecioFelix: `UPDATE [Indicadores].[dbo].[CtrlOper_PrecioFelix] 
              SET
              Fecha = @Fecha,
              Importe_PrecioFelix = @Importe_PrecioFelix,
              Asesor = @Asesor,
              Nombre_Cliente = @Nombre_Cliente,
              Factura_PrecioFelix = @Factura_PrecioFelix,
              Utilidad = @Utilidad,
              Comision_Asesor = @Comision_Asesor,
              Utilidad_Neta = @Utilidad_Neta
              
              WHERE
              Id = @Id`,

  /* QUERIES Ctrl Operativo Garanti Plus*/
  getCtrlOper_GarantiPlus: `SELECT 
          Id,
          Fecha,
          Importe_GarantiPlus,
          Costo_GarantiPlus,
          Asesor,
          Utilidad,
          Comision_Asesor,
          Utilidad_Neta

          FROM 
          [Indicadores].[dbo].[CtrlOper_GarantiPlus] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal ORDER BY Fecha DESC`,

  ExistsRegister_GarantiPlus: `SELECT * FROM [Indicadores].[dbo].[CtrlOper_GarantiPlus] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal 
          AND
          Fecha = @Fecha`,

  createCtrlOper_GarantiPlus: `INSERT INTO [Indicadores].[dbo].[CtrlOper_GarantiPlus] (
            Empresa,
            Sucursal,
            Fecha,
            Importe_GarantiPlus,
            Costo_GarantiPlus,
            Asesor,
            Utilidad,
            Comision_Asesor,
            Utilidad_Neta
            
            ) VALUES (
              @Empresa,
              @Sucursal,
              @Fecha,
              @Importe_GarantiPlus,
              @Costo_GarantiPlus,
              @Asesor,
              @Utilidad,
              @Comision_Asesor,
              @Utilidad_Neta
              
              );`,

  updateCtrlOper_GarantiPlus: `UPDATE [Indicadores].[dbo].[CtrlOper_GarantiPlus] 
              SET
              Fecha = @Fecha,
              Importe_GarantiPlus = @Importe_GarantiPlus,
              Costo_GarantiPlus = @Costo_GarantiPlus,
              Asesor = @Asesor,
              Utilidad = @Utilidad,
              Comision_Asesor = @Comision_Asesor,
              Utilidad_Neta = @Utilidad_Neta
              
              WHERE
              Id = @Id`,

  /* QUERIES Ctrl Operativo Seguros Inbros*/
  getCtrlOper_SegurosInbros: `SELECT 
          Id,
          Fecha,
          Importe_Inbros,
          Asesor,
          Utilidad,
          Comision_Asesor,
          Utilidad_Neta

          FROM 
          [Indicadores].[dbo].[CtrlOper_SegurosInbros] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal ORDER BY Fecha DESC`,

  ExistsRegister_SegurosInbros: `SELECT * FROM [Indicadores].[dbo].[CtrlOper_SegurosInbros] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal 
          AND
          Fecha = @Fecha`,

  createCtrlOper_SegurosInbros: `INSERT INTO [Indicadores].[dbo].[CtrlOper_SegurosInbros] (
            Empresa,
            Sucursal,
            Fecha,
            Importe_Inbros,
            Asesor,
            Utilidad,
            Comision_Asesor,
            Utilidad_Neta
            
            ) VALUES (
              @Empresa,
              @Sucursal,
              @Fecha,
              @Importe_Inbros,
              @Asesor,
              @Utilidad,
              @Comision_Asesor,
              @Utilidad_Neta
              
              );`,

  updateCtrlOper_SegurosInbros: `UPDATE [Indicadores].[dbo].[CtrlOper_SegurosInbros] 
              SET
              Fecha = @Fecha,
              Importe_Inbros = @Importe_Inbros,
              Asesor = @Asesor,
              Utilidad = @Utilidad,
              Comision_Asesor = @Comision_Asesor,
              Utilidad_Neta = @Utilidad_Neta
              
              WHERE
              Id = @Id`,

  // getCtrlOper_PrecioFelixContado
  /* QUERIES Ctrl Operativo Precio Felix Contado*/
  getCtrlOper_PrecioFelixContado: `SELECT 
          Id,
          Fecha,
          Importe_PrecioFelixContado,
          Asesor,
          Comision_Asesor

          FROM 
          [Indicadores].[dbo].[CtrlOper_PrecioFelixContado] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal ORDER BY Fecha DESC`,

  ExistsRegister_PrecioFelixContado: `SELECT * FROM [Indicadores].[dbo].[CtrlOper_PrecioFelixContado] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal 
          AND
          Fecha = @Fecha`,

  createCtrlOper_PrecioFelixContado: `INSERT INTO [Indicadores].[dbo].[CtrlOper_PrecioFelixContado] (
            Empresa,
            Sucursal,
            Fecha,
            Importe_PrecioFelixContado,
            Asesor,
            Comision_Asesor
            
            ) VALUES (
              @Empresa,
              @Sucursal,
              @Fecha,
              @Importe_PrecioFelixContado,
              @Asesor,
              @Comision_Asesor
              
              );`,

  updateCtrlOper_PrecioFelixContado: `UPDATE [Indicadores].[dbo].[CtrlOper_PrecioFelixContado] 
              SET
              Fecha = @Fecha,
              Importe_PrecioFelixContado = @Importe_PrecioFelixContado,
              Asesor = @Asesor,
              Comision_Asesor = @Comision_Asesor
              
              WHERE
              Id = @Id`,

  /* QUERIES Detalles Contratos GMF*/
  getDetallesContratosGMF: `SELECT 
          Id,
          Fecha,
          Folio_Contrato,
          Monto_ContratoAFinanciar,
          Nombre_Cliente,
          Utilidad

          FROM 
          [Indicadores].[dbo].[DetallesContratosGMF] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal ORDER BY Fecha DESC`,

  ExistsRegister_DetallesContratosGMF: `SELECT * FROM [Indicadores].[dbo].[DetallesContratosGMF] 
          WHERE
          Empresa = @Empresa
          AND
          Sucursal = @Sucursal 
          AND
          Fecha = @Fecha`,

  createDetallesContratosGMF: `INSERT INTO [Indicadores].[dbo].[DetallesContratosGMF] (
            Empresa,
            Sucursal,
            Fecha,
            Folio_Contrato,
            Monto_ContratoAFinanciar,
            Nombre_Cliente,
            Utilidad
            
            ) VALUES (
              @Empresa,
              @Sucursal,
              @Fecha,
              @Folio_Contrato,
              @Monto_ContratoAFinanciar,
              @Nombre_Cliente,
              @Utilidad
              
              );`,

  updateDetallesContratosGMF: `UPDATE [Indicadores].[dbo].[DetallesContratosGMF] 
              SET
              Fecha = @Fecha,
              Folio_Contrato = @Folio_Contrato,
              Monto_ContratoAFinanciar = @Monto_ContratoAFinanciar,
              Nombre_Cliente = @Nombre_Cliente,
              Utilidad = @Utilidad
              
              WHERE
              Id = @Id`,

  getAsesores: `SELECT Id, Nombre_Asesor, Activo, numvendedor FROM [Indicadores].[dbo].[Asesores_Ventas] 
  WHERE
  Empresa = @Empresa
  AND
  Sucursal = @Sucursal`,
  
  getAsesoresActivos: `SELECT Id, Nombre_Asesor FROM [Indicadores].[dbo].[Asesores_Ventas] 
  WHERE
  Empresa = @Empresa
  AND
  Sucursal = @Sucursal
  AND 
  Activo = 'S'
  `,

  getPenetracionGMF:`SELECT contratosgmf, entregas FROM [Indicadores].[dbo].[Presupuestos_indicadores] 
  WHERE
  Empresa = @Empresa
  AND
  Sucursal = @Sucursal
  AND 
  mes = @mes
  AND 
  periodo = @periodo`,

  getLeasingMes: `SELECT sol_contratos_comprados_leasing FROM 
  [Indicadores].[dbo].[reportes_diarios] 
  WHERE 
  MONTH(id_fecha) = @mes 
  AND 
  YEAR(id_fecha) = @periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal`,
  
  getUtilidadNetaGarantiaExtendidaGMPLUS: `SELECT Utilidad_Neta FROM 
  [Indicadores].[dbo].[CtrlOper_GarantiasGMPlus] 
  WHERE 
  MONTH(Fecha) = @Mes 
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal`,

  getUtilidadNetaGarantiaExtendidaGarantiPlus: `SELECT Utilidad_Neta FROM 
  [Indicadores].[dbo].[CtrlOper_GarantiPlus] 
  WHERE 
  MONTH(Fecha) = @Mes 
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal`,
  
  getUtilidadNetaOnStar: `SELECT Utilidad_Neta FROM 
  [Indicadores].[dbo].[CtrlOper_OnStar] 
  WHERE 
  MONTH(Fecha) = @Mes 
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal`,

  getUtilidadNetaAccesorios: `SELECT Utilidad_Neta FROM 
  [Indicadores].[dbo].[CtrlOper_Accesorios] 
  WHERE 
  MONTH(Fecha) = @Mes 
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal`,

  getUtilidadNetaGAP: `SELECT Utilidad_Neta FROM 
  [Indicadores].[dbo].[CtrlOper_GAP] 
  WHERE 
  MONTH(Fecha) = @Mes 
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal`,
  
  getUtilidadNetaSobrePreciosGMF: `SELECT Utilidad_Neta FROM 
  [Indicadores].[dbo].[CtrlOper_PrecioFelix] 
  WHERE 
  MONTH(Fecha) = @Mes 
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal`,
  
  getUtilidadNetaSegurosGMF: `SELECT Utilidad_Neta FROM 
  [Indicadores].[dbo].[CtrlOper_SeguroGMF] 
  WHERE 
  MONTH(Fecha) = @Mes 
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal`,

  getUtilidadNetaSegurosInbros: `SELECT Utilidad_Neta FROM 
  [Indicadores].[dbo].[CtrlOper_SegurosInbros] 
  WHERE 
  MONTH(Fecha) = @Mes 
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal`,
  
  getImporteSobrePreciosContado: `SELECT Importe_PrecioFelixContado FROM 
  [Indicadores].[dbo].[CtrlOper_PrecioFelixContado] 
  WHERE 
  MONTH(Fecha) = @Mes 
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal`,

  getImporteSeguroFromSeguroGMF: `SELECT Importe_Seguro FROM 
  [Indicadores].[dbo].[CtrlOper_SeguroGMF] 
  WHERE 
  MONTH(Fecha) = @Mes
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal
  AND
  Asesor = @Asesor`,
  
  getImporteInbrosFromSegurosInbros: `SELECT Importe_Inbros FROM 
  [Indicadores].[dbo].[CtrlOper_SegurosInbros] 
  WHERE 
  MONTH(Fecha) = @Mes
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal
  AND
  Asesor = @Asesor`,
  
  getImporteGarantiaFromGarantiasGMPlus: `SELECT Importe_Garantia FROM 
  [Indicadores].[dbo].[CtrlOper_GarantiasGMPlus] 
  WHERE 
  MONTH(Fecha) = @Mes
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal
  AND
  Asesor = @Asesor`,
  
  getImporteGAPFromGAP: `SELECT Importe_GAP FROM 
  [Indicadores].[dbo].[CtrlOper_GAP] 
  WHERE 
  MONTH(Fecha) = @Mes
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal
  AND
  Asesor = @Asesor`,
  
  getImporteOnStarFromOnStar: `SELECT Importe_OnStar FROM 
  [Indicadores].[dbo].[CtrlOper_OnStar] 
  WHERE 
  MONTH(Fecha) = @Mes
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal
  AND
  Asesor = @Asesor`,
  
  getImporteGarantiPlusFromGarantiPlus: `SELECT Importe_GarantiPlus FROM 
  [Indicadores].[dbo].[CtrlOper_GarantiPlus] 
  WHERE 
  MONTH(Fecha) = @Mes
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal
  AND
  Asesor = @Asesor`,
  
  getImporteAccesorioFromAccesorios: `SELECT Importe_Accesorio FROM 
  [Indicadores].[dbo].[CtrlOper_Accesorios] 
  WHERE 
  MONTH(Fecha) = @Mes
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal
  AND
  Asesor = @Asesor`,
  
  getImportePrecioFelixFromPrecioFelix: `SELECT Importe_PrecioFelix FROM 
  [Indicadores].[dbo].[CtrlOper_PrecioFelix] 
  WHERE 
  MONTH(Fecha) = @Mes
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal
  AND
  Asesor = @Asesor`,
  
  getImportePrecioFelixFromPrecioFelixContado: `SELECT Importe_PrecioFelixContado FROM 
  [Indicadores].[dbo].[CtrlOper_PrecioFelixContado] 
  WHERE 
  MONTH(Fecha) = @Mes
  AND 
  YEAR(Fecha) = @Periodo 
  AND 
  Empresa = @Empresa 
  AND 
  Sucursal = @Sucursal
  AND
  Asesor = @Asesor`,

  getFacturacionAsesores: `SELECT numvendedor, Vendedor, TipoVenta, Cant FROM
  [Indicadores].[dbo].[VentasEntregasDMS] 
  WHERE 
  Empresa = @Empresa
  AND
  Sucursal = @Sucursal
  AND 
  numvendedor = @numvendedor`,

  getEntregasMes: `SELECT numvendedor, Vendedor, TipoVenta, Cant FROM
  [Indicadores].[dbo].[VentasEntregasDMS] 
  WHERE 
  Empresa = @Empresa
  AND
  Sucursal = @Sucursal
  AND 
  numvendedor = @numvendedor
  AND
  TipoVenta = 'ENTREGA'`

}

/*  id_reporte int NOT NULL,
1.-     id_fecha ,
2.-     af_fresh_n ,
3.-     af_bback_n ,
4.-     af_primera_cita_n ,
5.-     af_digitales_n ,
6.-     sol_gmf_n ,
7.-     sol_bancos_n ,
8.-     sol_aprobaciones_gmf_n ,
9.-     sol_contratos_comprados_n ,
10.-    af_fresh_s ,
11.-    af_bback_s ,
12.-    af_primera_cita_s ,
13.-    af_digitales_s ,
14.-    sol_gmf_s ,
15.-    sol_bancos_s ,
16.-    sol_aprobaciones_gmf_s ,
17.-    sol_contratos_comprados_s ,
18.-    cit_agendadas_n ,
19.-    cit_cumplidas_n ,
20.-    cit_primera_cita_n ,
21.-    cit_digitales_n , 
 */
import sql from 'mssql'

import config from "../config"

const dbSettings = {
    user: config.dbUser,
    password: config.dbPassword,
    server: config.dbServer, //se encuentra en instance name
    database: config.dbDatabase,
    //port: '#puerto' //se puede asignar el puerto, por defecto ya contiene uno

    options: {
        encrypt: false, // for azure
        trustServerCertificate: true, // change to true for local dev / self-signed certs
    },

};

export const getConnection = async () => {
    try {
        const pool = await sql.connect(dbSettings);
        return pool;
    } catch (error) {
        console.log(error);
    }
};

export const FechaDeHoy = () => {
    let Fecha = new Date();
    let Mes = Fecha.getMonth() + 1;
    var Dia = Fecha.getDate();
    var Anio = Fecha.getFullYear();      
    
    return `${Anio}-` + `${Mes.toString().length > 1? Mes: '0' + Mes}-${Dia.toString().length > 1? Dia: '0' + Dia}`;
    
}

export const CurrentMonth = () => {
    let Fecha = new Date();
    let Mes = Fecha.getMonth() + 1;

    return `${Mes.toString().length > 1? Mes: '0' + Mes}`;

}

export const CurrentYear = () => {
    let Fecha = new Date();
    var Anio = Fecha.getFullYear();

    return Anio;

}

export { sql };
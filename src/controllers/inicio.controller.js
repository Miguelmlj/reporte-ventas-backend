import { getConnection, querys, sql, FechaDeHoy, CurrentMonth, CurrentYear } from "../database";

export const obtenerReporteDiario = async (req, res) => {
    let currentDate = FechaDeHoy();
    const { user, empresa, sucursal } = req.body;
    // console.log(user);
    let cuery = querys.getReporteDiarioByFecha;

    
    try {
        const pool = await getConnection();

        const result = await pool.request()
            .input("id_fecha", sql.Date, currentDate)
            .input("empresa", sql.Int, empresa)
            .input("sucursal", sql.Int, sucursal)
            .query(cuery);

        res.json(result.recordset);

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

export const obtenerNombreAgencia = async (req, res) => {
    const {empresa, sucursal } = req.body;
    let cuery = querys.getNombreAgencia;

    try {
        const pool = await getConnection();

        const result = await pool.request()
            .input("empresa", sql.Int, empresa)
            .input("sucursal", sql.Int, sucursal)
            .query(cuery);

        res.json(result.recordset[0]);

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

export const obtenerAcumuladoDelMes = async (req, res) => {
    let month = CurrentMonth();
    let year = CurrentYear();
    const { user, empresa, sucursal } = req.body;
    
    let cuery = querys.getAcumuladoDelMes;

    
    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("month", sql.Int, month)
            .input("year", sql.Int, year)
            .input("empresa", sql.Int, empresa)
            .input("sucursal", sql.Int, sucursal)
            .query(cuery);


        //VALIDAR ANTES DE REALIZAR ESTO, SI EL ARREGLO QUE TRAEMOS NO ES VACÍO
        //Haremos un sólo arreglo donde estarán las sumas de cada propiedad de los registros del mes
        let nuevoArray = {
            "af_fresh_n": result.recordset.map(item => item.af_fresh_n).reduce((prev, curr) => prev + curr, 0),

            "af_bback_n": result.recordset.map(item => item.af_bback_n).reduce((prev, curr) => prev + curr, 0),

            "af_primera_cita_n": result.recordset.map(item => item.af_primera_cita_n).reduce((prev, curr) => prev + curr, 0),

            "af_digitales_n": result.recordset.map(item => item.af_digitales_n).reduce((prev, curr) => prev + curr, 0),

            "sol_gmf_n": result.recordset.map(item => item.sol_gmf_n).reduce((prev, curr) => prev + curr, 0),

            "sol_bancos_n": result.recordset.map(item => item.sol_bancos_n).reduce((prev, curr) => prev + curr, 0),

            "sol_aprobaciones_gmf_n": result.recordset.map(item => item.sol_aprobaciones_gmf_n).reduce((prev, curr) => prev + curr, 0),

            "sol_contratos_comprados_n": result.recordset.map(item => item.sol_contratos_comprados_n).reduce((prev, curr) => prev + curr, 0),

            "af_fresh_s": result.recordset.map(item => item.af_fresh_s).reduce((prev, curr) => prev + curr, 0),

            "af_bback_s": result.recordset.map(item => item.af_bback_s).reduce((prev, curr) => prev + curr, 0),

            "af_primera_cita_s": result.recordset.map(item => item.af_primera_cita_s).reduce((prev, curr) => prev + curr, 0),

            "af_digitales_s": result.recordset.map(item => item.af_digitales_s).reduce((prev, curr) => prev + curr, 0),

            "sol_gmf_s": result.recordset.map(item => item.sol_gmf_s).reduce((prev, curr) => prev + curr, 0),

            "sol_bancos_s": result.recordset.map(item => item.sol_bancos_s).reduce((prev, curr) => prev + curr, 0),

            "sol_aprobaciones_gmf_s": result.recordset.map(item => item.sol_aprobaciones_gmf_s).reduce((prev, curr) => prev + curr, 0),

            "sol_contratos_comprados_s": result.recordset.map(item => item.sol_contratos_comprados_s).reduce((prev, curr) => prev + curr, 0),

            "cit_agendadas_n": result.recordset.map(item => item.cit_agendadas_n).reduce((prev, curr) => prev + curr, 0),

            "cit_cumplidas_n": result.recordset.map(item => item.cit_cumplidas_n).reduce((prev, curr) => prev + curr, 0),

            "cit_primera_cita_n": result.recordset.map(item => item.cit_primera_cita_n).reduce((prev, curr) => prev + curr, 0),

            "cit_digitales_n": result.recordset.map(item => item.cit_digitales_n).reduce((prev, curr) => prev + curr, 0),

        }

        
        //SI NO HAY REGISTROS DEL MES ACTUAL, SE COLOCARA UN 0 EN CADA PROPIEDAD DEL OBJETO

        res.json(nuevoArray);

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

    // res.json({month: CurrentMonth(), year: CurrentYear()})
}
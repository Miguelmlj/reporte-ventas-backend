import { getConnection, querys, sql } from "../database"

export const getGAP = async (req, res) => {
    const { Empresa, Sucursal } = req.body;

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getCtrlOper_GAP)

        res.json(result.recordset)

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

export const createGAP = async (req, res) => {
    const { GAP, agencia } = req.body
    let IsRegisterInBD = false;

    /* try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, GAP.Fecha)
            .query(querys.ExistsRegister_GAP)

        if (result.rowsAffected > 0) IsRegisterInBD = true;
        if (result.rowsAffected < 1) { */

    try {
        const pool = await getConnection();
        insertRegisterToBD(pool, GAP, agencia);
        
        if (!IsRegisterInBD) res.json({ isCreated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }


    /* }

    if (IsRegisterInBD) res.json({ isCreated: false })
    if (!IsRegisterInBD) res.json({ isCreated: true })

} catch (error) {
    res.status(500);
    res.send(error.message);

} */
}

const insertRegisterToBD = async (pool, GAP, agencia) => {

    await pool.request()
        .input("Empresa", sql.Int, agencia.Empresa)
        .input("Sucursal", sql.Int, agencia.Sucursal)
        .input("Fecha", sql.VarChar, GAP.Fecha)
        .input("Importe_GAP", sql.Float, GAP.Importe_GAP)
        .input("Asesor", sql.VarChar, GAP.Asesor)
        .input("Nombre_Cliente", sql.VarChar, GAP.Nombre_Cliente)
        .input("Utilidad", sql.Float, GAP.Utilidad)
        .input("Comision_Asesor", sql.Float, GAP.Comision_Asesor)
        .input("Utilidad_Neta", sql.Float, GAP.Utilidad_Neta)

        .query(querys.createCtrlOper_GAP)

}

export const updateGAP = async (req, res) => {
    const { GAP } = req.body

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Fecha", sql.VarChar, GAP.Fecha)
            .input("Importe_GAP", sql.Float, GAP.Importe_GAP)
            .input("Asesor", sql.VarChar, GAP.Asesor)
            .input("Nombre_Cliente", sql.VarChar, GAP.Nombre_Cliente)
            .input("Utilidad", sql.Float, GAP.Utilidad)
            .input("Comision_Asesor", sql.Float, GAP.Comision_Asesor)
            .input("Utilidad_Neta", sql.Float, GAP.Utilidad_Neta)
            .input("Id", sql.Int, GAP.Id)

            .query(querys.updateCtrlOper_GAP)

        res.json({ isUpdated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

}
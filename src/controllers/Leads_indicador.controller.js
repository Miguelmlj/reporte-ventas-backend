import { getConnection, querys, sql } from "../database"

export const createIndicadorLeads = async (req, res) => {
    const { indicadorLeads, agencia } = req.body

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, indicadorLeads.Fecha)
            .input("Oport_registradasPeriodo", sql.Int, indicadorLeads.Oport_registradasPeriodo)
            .input("Oport_contactadas", sql.Int, indicadorLeads.Oport_contactadas)
            .input("Oport_contactadasATiempo", sql.Int, indicadorLeads.Oport_contactadasATiempo)
            .input("Porcen_contactadosATiempo", sql.Float, indicadorLeads.Porcen_contactadosATiempo)
            .query(querys.createLeads_indicador)

        res.json({ isCreated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

export const getIndicadorLeads = async (req, res) => {
    const { Empresa, Sucursal } = req.body;

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getLeads_indicador)

        res.json(result.recordset)

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

export const updateIndicadorLeads = async (req, res) => {
    const { indicadorLeads } = req.body

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Fecha", sql.VarChar, indicadorLeads.Fecha)
            .input("Oport_registradasPeriodo", sql.Int, indicadorLeads.Oport_registradasPeriodo)
            .input("Oport_contactadas", sql.Int, indicadorLeads.Oport_contactadas)
            .input("Oport_contactadasATiempo", sql.Int, indicadorLeads.Oport_contactadasATiempo)
            .input("Porcen_contactadosATiempo", sql.Float, indicadorLeads.Porcen_contactadosATiempo)
            .input("Id", sql.Int, indicadorLeads.Id)
            .query(querys.updateLeads_indicador)

        res.json({ isUpdated: true })


    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}
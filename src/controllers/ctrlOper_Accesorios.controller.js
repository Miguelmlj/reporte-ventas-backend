import { getConnection, querys, sql } from "../database"

export const getAccesorios = async (req, res) => {
    const { Empresa, Sucursal } = req.body;

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getCtrlOper_Accesorios)

        res.json(result.recordset)

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

}

export const createAccesorios = async (req, res) => {
    const { Accesorio, agencia } = req.body
    let IsRegisterInBD = false;

    /* try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, Accesorio.Fecha)
            .query(querys.ExistsRegister_Accesorios)

        if (result.rowsAffected > 0) IsRegisterInBD = true;
        if (result.rowsAffected < 1) { */

            try {
                const pool = await getConnection();
                insertRegisterToBD(pool, Accesorio, agencia);
                if (!IsRegisterInBD) res.json({ isCreated: true })

            } catch (error) {
                res.status(500);
                res.send(error.message);
            }

       /*  }

        if (IsRegisterInBD) res.json({ isCreated: false })
        if (!IsRegisterInBD) res.json({ isCreated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    } */

}

const insertRegisterToBD = async (pool, Accesorio, agencia) => {

    await pool.request()
        .input("Empresa", sql.Int, agencia.Empresa)
        .input("Sucursal", sql.Int, agencia.Sucursal)
        .input("Fecha", sql.VarChar, Accesorio.Fecha)
        .input("Importe_Accesorio", sql.Float, Accesorio.Importe_Accesorio)
        .input("Descrip_Accesorio", sql.VarChar, Accesorio.Descrip_Accesorio)
        .input("Codigo_Accesorio", sql.VarChar, Accesorio.Codigo_Accesorio)
        .input("Costo_Accesorio", sql.Float, Accesorio.Costo_Accesorio)
        .input("Asesor", sql.VarChar, Accesorio.Asesor)
        .input("Nombre_Cliente", sql.VarChar, Accesorio.Nombre_Cliente)
        .input("Utilidad", sql.Float, Accesorio.Utilidad)
        .input("Comision_Asesor", sql.Float, Accesorio.Comision_Asesor)
        .input("Utilidad_Neta", sql.Float, Accesorio.Utilidad_Neta)

        .query(querys.createCtrlOper_Accesorios)

}

export const updateAccesorios = async (req, res) => {
    
    const { Accesorio } = req.body

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Fecha", sql.VarChar, Accesorio.Fecha)
            .input("Importe_Accesorio", sql.Float, Accesorio.Importe_Accesorio)
            .input("Descrip_Accesorio", sql.VarChar, Accesorio.Descrip_Accesorio)
            .input("Codigo_Accesorio", sql.VarChar, Accesorio.Codigo_Accesorio)
            .input("Costo_Accesorio", sql.Float, Accesorio.Costo_Accesorio)
            .input("Asesor", sql.VarChar, Accesorio.Asesor)
            .input("Nombre_Cliente", sql.VarChar, Accesorio.Nombre_Cliente)
            .input("Utilidad", sql.Float, Accesorio.Utilidad)
            .input("Comision_Asesor", sql.Float, Accesorio.Comision_Asesor)
            .input("Utilidad_Neta", sql.Float, Accesorio.Utilidad_Neta)
            .input("Id", sql.Int, Accesorio.Id)

            .query(querys.updateCtrlOper_Accesorios)

        res.json({ isUpdated: true })
        
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

}
import { getConnection, querys, sql } from "../database";

//obtener todos los registros de afluencia (nuevos y seminuevos)
export const getAfluencia = async (req, res) => {
  const { empresa, sucursal } = req.body;
  let cuery = querys.getAllAfluencia;

  try {
    const pool = await getConnection();
    const result = await pool.request()
      .input("empresa", sql.Int, empresa)
      .input("sucursal", sql.Int, sucursal)
      .query(cuery);
    res.json(result.recordset);
  } catch (error) {
    res.status(500);
    res.send(error.message);
  }
}

//obtener afluencia por fecha / comprobar si existe o no un registro con la fecha
//esta función se repite en todos los módulos pero servirá en un futuro para hacer modificaciones
export const getAfluenciaByFecha = async (req, res) => {
  const { id_fecha, empresa, sucursal, user, tipo_usuario } = req.body;

  //se creará getAfluenciaByFecha nuevos y getAfluenciaByFecha seminuevos
  //getAfluenciaByFecha; //general será para el admin

  let cuery = getCueryAfluenciaByFecha(tipo_usuario)

  try {
    const pool = await getConnection();

    const result = await pool
      .request()

      .input("id_fecha", sql.VarChar, id_fecha)
      .input("empresa", sql.Int, empresa)
      .input("sucursal", sql.Int, sucursal)
      .query(cuery);

    if (result.rowsAffected > 0) {
      res.json({ message: "registrocreado" });
    } else {
      res.json({ message: "registronocreado" });

    }

  } catch (error) {
    res.status(500);
    res.send(error.message);
  }

};


//FUNCIÓN HELPER - RETORNA SI ES ADMIN O HOSTER NUEVO O SEMINUEVO
const getCueryAfluenciaByFecha = (tipo_usuario) => {
  //extraer la inicial, si es a o h
  let cuery = ''

  if (tipo_usuario === '1') {
    cuery = querys.getAfluenciaByFecha;
  } else if (tipo_usuario === '2') {
    cuery = querys.getAfluenciaByFechaNuevos;
  } else if (tipo_usuario === '3') {
    cuery = querys.getAfluenciaByFechaSeminuevos;
  }

  return cuery;
}

const getCueryCreateAfluencia = (tipo_usuario) => {
  let cuery = ''
  if (tipo_usuario === '1') {
    cuery = querys.createAfluencia; //administrador
  } else if (tipo_usuario === '2') {
    cuery = querys.createAfluenciaNuevos; //hostess nuevos
  } else if (tipo_usuario === '3') {
    cuery = querys.createAfluenciaSeminuevos; //hostess seminuevos
  }


  return cuery;

}

const getCueryUpdateAfluencia = (tipo_usuario) => {
  let cuery = ''
  if (tipo_usuario === '1') {
    cuery = querys.updateAfluenciaByFecha;
  } else if (tipo_usuario === '2') {
    cuery = querys.updateAfluenciaByFechaNuevos;
  } else if (tipo_usuario === '3') {
    cuery = querys.updateAfluenciaByFechaSeminuevos;
  }


  return cuery;
}

//crear afluencia
export const createAfluencia = async (req, res) => {

  const {
    user,
    id_fecha,
    af_fresh_n,
    af_bback_n,
    af_primera_cita_n,
    af_digitales_n,

    af_fresh_s,
    af_bback_s,
    af_primera_cita_s,
    af_digitales_s,

    empresa,
    sucursal,
    tipo_usuario
  } = req.body;

  let cuery = getCueryCreateAfluencia(tipo_usuario);


  try {
    const pool = await getConnection();
    /*  await pool
       .request()
       .input("empresa", sql.Int, empresa)
       .input("sucursal", sql.Int, sucursal)
       .input("id_fecha", sql.Date, id_fecha)
       .input("af_fresh_n", sql.Int, af_fresh_n)
       .input("af_bback_n", sql.Int, af_bback_n)
       .input("af_primera_cita_n", sql.Int, af_primera_cita_n)
       .input("af_digitales_n", sql.Int, af_digitales_n)
 
       .input("af_fresh_s", sql.Int, af_fresh_s)
       .input("af_bback_s", sql.Int, af_bback_s)
       .input("af_primera_cita_s", sql.Int, af_primera_cita_s)
       .input("af_digitales_s", sql.Int, af_digitales_s)
 
       .query(cuery); */

    //SE VALIDA QUE TIPO DE USUARIO ES, PARA RETORNAR LOS VALORES EN EL JSON
    if (tipo_usuario === '1') {
      await pool
        .request()
        .input("empresa", sql.Int, empresa)
        .input("sucursal", sql.Int, sucursal)
        .input("id_fecha", sql.Date, id_fecha)
        .input("af_fresh_n", sql.Int, af_fresh_n)
        .input("af_bback_n", sql.Int, af_bback_n)
        .input("af_primera_cita_n", sql.Int, af_primera_cita_n)
        .input("af_digitales_n", sql.Int, af_digitales_n)

        .input("af_fresh_s", sql.Int, af_fresh_s)
        .input("af_bback_s", sql.Int, af_bback_s)
        .input("af_primera_cita_s", sql.Int, af_primera_cita_s)
        .input("af_digitales_s", sql.Int, af_digitales_s)

        .query(cuery);

      res.json({
        id_fecha,
        af_fresh_n,
        af_bback_n,
        af_primera_cita_n,
        af_digitales_n,

        af_fresh_s,
        af_bback_s,
        af_primera_cita_s,
        af_digitales_s,

      });

    } else if (tipo_usuario === '2') {
      await pool
        .request()
        .input("empresa", sql.Int, empresa)
        .input("sucursal", sql.Int, sucursal)
        .input("id_fecha", sql.Date, id_fecha)
        .input("af_fresh_n", sql.Int, af_fresh_n)
        .input("af_bback_n", sql.Int, af_bback_n)
        .input("af_primera_cita_n", sql.Int, af_primera_cita_n)
        .input("af_digitales_n", sql.Int, af_digitales_n)

        .query(cuery);

      res.json({
        id_fecha,
        af_fresh_n,
        af_bback_n,
        af_primera_cita_n,
        af_digitales_n,

      });

    } else if (tipo_usuario === '3') {
      await pool
        .request()
        .input("empresa", sql.Int, empresa)
        .input("sucursal", sql.Int, sucursal)
        .input("id_fecha", sql.Date, id_fecha)

        .input("af_fresh_s", sql.Int, af_fresh_s)
        .input("af_bback_s", sql.Int, af_bback_s)
        .input("af_primera_cita_s", sql.Int, af_primera_cita_s)
        .input("af_digitales_s", sql.Int, af_digitales_s)

        .query(cuery);

      res.json({
        id_fecha,
        af_fresh_s,
        af_bback_s,
        af_primera_cita_s,
        af_digitales_s,

      });

    }


  } catch (error) {
    /* console.log(error);
    console.log(error.message); */
    res.status(500);
    res.send(error.message);
  }

}

//actualizar registro por fecha, se utiliza un patch para hacer cambios parciales en la tabla, campos de AFLUENCIA específicos
export const updateAfluenciaByFecha = async (req, res) => {
  const {
    id_fecha,
    af_fresh_n,
    af_bback_n,
    af_primera_cita_n,
    af_digitales_n,

    af_fresh_s,
    af_bback_s,
    af_primera_cita_s,
    af_digitales_s,

    empresa,
    sucursal,
    tipo_usuario
  } = req.body;

  let cuery = getCueryUpdateAfluencia(tipo_usuario)


  try {
    const pool = await getConnection();
    /* await pool
      .request()
      .input("id_fecha", sql.Date, id_fecha)
      .input("af_fresh_n", sql.Int, af_fresh_n)
      .input("af_bback_n", sql.Int, af_bback_n)
      .input("af_primera_cita_n", sql.Int, af_primera_cita_n)
      .input("af_digitales_n", sql.Int, af_digitales_n)

      .input("af_fresh_s", sql.Int, af_fresh_s)
      .input("af_bback_s", sql.Int, af_bback_s)
      .input("af_primera_cita_s", sql.Int, af_primera_cita_s)
      .input("af_digitales_s", sql.Int, af_digitales_s)
      .input("empresa", sql.Int, empresa)
      .input("sucursal", sql.Int, sucursal)

      .query(cuery); */

    if (tipo_usuario === '1') {
      await pool
      .request()
      .input("id_fecha", sql.Date, id_fecha)
      .input("af_fresh_n", sql.Int, af_fresh_n)
      .input("af_bback_n", sql.Int, af_bback_n)
      .input("af_primera_cita_n", sql.Int, af_primera_cita_n)
      .input("af_digitales_n", sql.Int, af_digitales_n)

      .input("af_fresh_s", sql.Int, af_fresh_s)
      .input("af_bback_s", sql.Int, af_bback_s)
      .input("af_primera_cita_s", sql.Int, af_primera_cita_s)
      .input("af_digitales_s", sql.Int, af_digitales_s)
      .input("empresa", sql.Int, empresa)
      .input("sucursal", sql.Int, sucursal)

      .query(cuery);

      res.json({
        id_fecha,
        af_fresh_n,
        af_bback_n,
        af_primera_cita_n,
        af_digitales_n,
  
        af_fresh_s,
        af_bback_s,
        af_primera_cita_s,
        af_digitales_s,
  
      });

    } else if (tipo_usuario === '2') {
      await pool
      .request()
      .input("id_fecha", sql.Date, id_fecha)
      .input("af_fresh_n", sql.Int, af_fresh_n)
      .input("af_bback_n", sql.Int, af_bback_n)
      .input("af_primera_cita_n", sql.Int, af_primera_cita_n)
      .input("af_digitales_n", sql.Int, af_digitales_n)
      .input("empresa", sql.Int, empresa)
      .input("sucursal", sql.Int, sucursal)

      .query(cuery);

      res.json({
        id_fecha,
        af_fresh_n,
        af_bback_n,
        af_primera_cita_n,
        af_digitales_n,
  
      });

    } else if (tipo_usuario === '3') {
      await pool
      .request()
      .input("id_fecha", sql.Date, id_fecha)
      .input("af_fresh_s", sql.Int, af_fresh_s)
      .input("af_bback_s", sql.Int, af_bback_s)
      .input("af_primera_cita_s", sql.Int, af_primera_cita_s)
      .input("af_digitales_s", sql.Int, af_digitales_s)
      .input("empresa", sql.Int, empresa)
      .input("sucursal", sql.Int, sucursal)

      .query(cuery);

      res.json({
        id_fecha,
        af_fresh_s,
        af_bback_s,
        af_primera_cita_s,
        af_digitales_s,
  
      });

    }

    
  } catch (error) {
    console.log(error);
    console.log(error.message);
    res.status(500);
    res.send(error.message);
  }

}
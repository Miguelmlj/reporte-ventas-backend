import { getConnection, querys, sql} from "../database";
import axios from 'axios'

export const getUsuarios = async (req, res) => { //este nombre de arrow function es temporal
  try {
    const pool = await getConnection();
    const result = await pool.request().query(querys.getAllUsuarios);
    res.json(result.recordset);
  } catch (error) {
    res.status(500);
    res.send(error.message);
  }
}

export const createUsuarios = async (req, res) => {
  const { nombre_usuario, clave, empresa, sucursal, tipo_usuario, responsable } = req.body;

  try {
    const pool = await getConnection();

    await pool
      .request()
      .input("nombre_usuario", sql.VarChar, nombre_usuario)
      .input("clave", sql.VarChar, clave)
      .input("empresa", sql.Int, empresa)
      .input("sucursal", sql.Int, sucursal)
      .input("tipo_usuario", sql.Int, tipo_usuario)
      .input("responsable", sql.Int, responsable)
      .query(querys.addUsuario);

    res.json({
      usuarios: {
        nombre_usuario,
        clave
      }

    })
  } catch (error) {
    res.status(500);
    res.send(error.message);
  }
}

export const getUsuariosByNombre = async (req, res) => {
  const { nombre_usuario } = req.body;

  try {
    const pool = await getConnection();

    const result = await pool
      .request()
      .input("nombre_usuario", sql.VarChar, nombre_usuario)
      .query(querys.getUsuarioByNombre);

    if (result.rowsAffected > 0) {
      res.json({ message: "usuarioexiste" });
    } else {
      res.json({ message: "usuarionoexiste" });

    }

  } catch (error) {
    res.status(500);
    res.send(error.message);

  }
}

export const getUsuarioById = async (req, res) => {
  const { id } = req.params;

  try {
    const pool = await getConnection();
    const result = await pool.request()
      .input("id_usuario", sql.Int, id)
      .query(querys.getUserById)
        res.json(result.recordset)

  } catch (error) {
    res.status(500);
    res.send(error.message)

  }
}

export const updateUsuario = async (req, res) => {
  const {
    id_usuario,
    tipo_usuario,
    responsable,
    inicio,
    afluencia,
    citas,
    solicitudes,
    anticipos,
    invfisico,
    codigosQr,
    usuarios,
    indFYI,
    indLeads,
    indCentroProp,
    indJDPower,
    indBDC,
    indMkt,
    CO_SegGMF,
    CO_GarGMPlus,
    CO_OnStrt,
    CO_GAP,
    CO_Accsrios,
    CO_PrecioFlx,
    CO_GarPlus,
    CO_SegInbros,
    CO_PrecioFlxCntdo,
    DtllsCntrtsGMF,
    CO_Resumen,
    Asig_Contratos,
    Clientes_Flotillas,
    OrdenDeCompra_F
  } = req.body;

  try {
    const pool = await getConnection();
    await pool
      .request()
      .input("id_usuario", sql.Int, id_usuario)
      .input("tipo_usuario", sql.Int, tipo_usuario)
      .input("responsable", sql.Int, responsable)
      .input("inicio", sql.VarChar, inicio)
      .input("afluencia", sql.VarChar, afluencia)
      .input("citas", sql.VarChar, citas)
      .input("solicitudes", sql.VarChar, solicitudes)
      .input("anticipos", sql.VarChar, anticipos)
      .input("invfisico", sql.VarChar, invfisico)
      .input("codigosQr", sql.VarChar, codigosQr)
      .input("usuarios", sql.VarChar, usuarios)
      .input("indFYI", sql.VarChar, indFYI)
      .input("indLeads", sql.VarChar, indLeads)
      .input("indCentroProp", sql.VarChar, indCentroProp)
      .input("indJDPower", sql.VarChar, indJDPower)
      .input("indBDC", sql.VarChar, indBDC)
      .input("indMkt", sql.VarChar, indMkt)
      .input("CO_SegGMF", sql.VarChar, CO_SegGMF)
      .input("CO_GarGMPlus", sql.VarChar, CO_GarGMPlus)
      .input("CO_OnStrt", sql.VarChar, CO_OnStrt)
      .input("CO_GAP", sql.VarChar, CO_GAP)
      .input("CO_Accsrios", sql.VarChar, CO_Accsrios)
      .input("CO_PrecioFlx", sql.VarChar, CO_PrecioFlx)
      .input("CO_GarPlus", sql.VarChar, CO_GarPlus)
      .input("CO_SegInbros", sql.VarChar, CO_SegInbros)
      .input("CO_PrecioFlxCntdo", sql.VarChar, CO_PrecioFlxCntdo)
      .input("DtllsCntrtsGMF", sql.VarChar, DtllsCntrtsGMF)
      .input("CO_Resumen", sql.VarChar, CO_Resumen)
      .input("Asig_Contratos", sql.VarChar, Asig_Contratos)
      .input("Clientes_Flotillas", sql.VarChar, Clientes_Flotillas)
      .input("OrdenDeCompra_F", sql.VarChar, OrdenDeCompra_F)

      .query(querys.updatePermissionsUser)

    res.json({ isUpdated: true })

  } catch (error) {
    res.status(500);
    res.send(error.message);
  }
}

export const ExistsUsersDMS = async (req, res) => {
  try {
    const pool = await getConnection();
    const result = await pool
      .request()
      .query(querys.ExistsUsersDMS)

    if (result.rowsAffected > 0) {
      res.json({ message: "existen" });
    } else {
      res.json({ message: "noexisten" });
    }

  } catch (error) {
    res.status(500);
    res.send(error.message);
  }
}

export const CreateUsersDMS = async (req, res) => {  
  const objectReceived = await axiosUsersDMS()
  const usersOfDMS = objectReceived['data'];
  const Sucursal = 1;
  const DMS = 1;

  try {
    const pool = await getConnection();

    if (usersOfDMS.length > 0) {
      
      for (const field of usersOfDMS) {
        await pool.request()
        .input("Empresa", sql.Int, field.EMPRESA)
        .input("Sucursal", sql.Int, Sucursal)
        .input("Usuario", sql.VarChar, field.USUARIO)
        .input("Nombre", sql.VarChar, field.NOMBRE)
        .input("Clave", sql.VarChar, field.CLAVE)
        .input("Puesto", sql.VarChar, field.PUESTO)
        .input("DMS", sql.Int, DMS)
        
          .query(querys.CreateUsersDMS)
        }        
        res.json({ isCreated: true })        
      } else{      
      res.json({ isCreated: false })
    }

  } catch (error) {
    res.status(500);
    res.send(error.message);
    console.log(error.message);
  }
}

export const DeleteUsersDMS = async (req, res) => {
  try {
    const pool = await getConnection();
    await pool.request()
      .query(querys.DeleteUsersDMS);

    // await pool.request()
    //   .query(querys.ResetIdUsuarioIncrementalUsersDMS)

    res.json({
      isDeleted: true
    })

  } catch (error) {
    res.status(500);
    res.send(error.message);
  }

}

export const getUserRols = async (req, res) => {
  try {
    const pool = await getConnection();
    const result = await pool.request().query(querys.getUserRols);
    res.json(result.recordset);
  } catch (error) {
    res.status(500);
    res.send(error.message);
  }
}

export const InsertNewUsers = async (req, res) => {
  const {UserRols } = req.body;
  const objectReceived = await axiosUsersDMS()
  const UsersDMS = objectReceived['data'];
  const Sucursal = 1;
  const TipoUsuarioDefault = 1;

  //LISTAS CON PROBABLES VALORES DE LOS PUESTOS.
  const gerenteNames = [
    "GERENTE",
    "GTE",
    "GERENTE BDC",
    "GERENTE SERVICIO", "GTE SERVICIO",
    "GTE REF", "GERENTE GRAL",
    "GTE REFACCIONES",
    "GTE ADMINISTRATIVO", "GTE ADMIVO",
    "GERENTE SEMINUEVOS",
    "GERENTE DE POSTVENTA",
    "GTE GRAL", "GTE SERV CORP",
    "GTE VENTAS", "GTE VENTAS AERO"
  ];
  const asesorVentasNames = [
    "ASESOR VENTAS", "ASEOSR DE VENTAS",
    "ASESOR DE VENTAS", "VENTAS MOSTRADOR",
    "VENTAS", "VENTAS AERO",
    "BAJA - ASESOR DE VENTAS"
  ];
  const FYINames = ["F&I", "BAJA - F&I"];
  const BDCNames = [
    "BDC",
    "BDC CADILLAC CLN",
    "BDC CULIACAN"
  ];
  const HostessNames = ["HOSSTES", "HOSTESS", "HOSTESS VENTAS"];
  const AuxiliarContableNames = [
    "AUX CONTABLE",
    "AUXILIAR CONTABLE",
    "CONTADOR",
    "AUXILIAR CONTABLE NOMINAS",
    "CONTADOR CADILLAC",
    "CONTABILIDAD",
    "CONTADOR ZAPATA"
  ];

  const listaEncapsuladaPuestos = [
    gerenteNames, 
    asesorVentasNames,
    FYINames,
    BDCNames,
    HostessNames,
    AuxiliarContableNames
  ]

  // Realizar consulta sql para verificar que el usuario no existe: entonces insertarlo.
  
  try {
    const pool = await getConnection();
    
    for (const UserDMS of UsersDMS) {
      const result = await pool
        .request()
        .input("nombre", sql.VarChar, UserDMS.USUARIO.trim())
        .input("Empresa", sql.Int, UserDMS.EMPRESA)
        .input("Sucursal", sql.Int, Sucursal)
        .query(querys.ExistsUserOnTable);

      if (result.rowsAffected > 0) {
        // en este punto no se agrega el usuario, porque ya se encuentra en la bd

      } else {
        const responsableAndPermissionObj = getPermisos(listaEncapsuladaPuestos, UserDMS, UserRols)

        try {
          insertUserToBD(
            pool,
            UserDMS,
            Sucursal,
            responsableAndPermissionObj.responsable,
            responsableAndPermissionObj.setPermissionToUser,
            TipoUsuarioDefault
          );

        } catch (error) {
          res.status(500);
          res.send(error.message);
        }

      }//else rowsAffected

    }  

    // res.json({responsable: responsable, permisos: setPermissionToUser})
    res.json({ isSinchronizationCompleted: true })

  } catch (error) {
    res.status(500);
    res.send(error.message);
  }
}

const insertUserToBD = async (
  pool,
  objUserDMS,
  Sucursal,
  responsable,
  setPermissionToUser,
  TipoUsuarioDefault
) => {

  await pool
    .request()
    .input("Empresa", sql.Int, objUserDMS.EMPRESA)
    .input("Sucursal", sql.Int, Sucursal)
    .input("nombre_usuario", sql.VarChar, objUserDMS.USUARIO)
    .input("clave", sql.VarChar, objUserDMS.CLAVE)
    .input("tipo_usuario", sql.Int, TipoUsuarioDefault)
    .input("responsable", sql.Int, responsable)
    .input("inicio", sql.VarChar, setPermissionToUser.inicio)
    .input("afluencia", sql.VarChar, setPermissionToUser.afluencia)
    .input("citas", sql.VarChar, setPermissionToUser.citas)
    .input("solicitudes", sql.VarChar, setPermissionToUser.solicitudes)
    .input("anticipos", sql.VarChar, setPermissionToUser.anticipos)
    .input("invfisico", sql.VarChar, setPermissionToUser.invfisico)
    .input("codigosQr", sql.VarChar, setPermissionToUser.codigosQr)
    .input("Usuarios", sql.VarChar, setPermissionToUser.usuarios)

    .input("indFYI", sql.VarChar, setPermissionToUser.indFYI)
    .input("indLeads", sql.VarChar, setPermissionToUser.indLeads)
    .input("indCentroProp", sql.VarChar, setPermissionToUser.indCentroProp)
    .input("indJDPower", sql.VarChar, setPermissionToUser.indJDPower)
    .input("indBDC", sql.VarChar, setPermissionToUser.indBDC)
    .input("indMkt", sql.VarChar, setPermissionToUser.indMkt)

    .input("CO_SegGMF", sql.VarChar, setPermissionToUser.CO_SegGMF)
    .input("CO_GarGMPlus", sql.VarChar, setPermissionToUser.CO_GarGMPlus)
    .input("CO_OnStrt", sql.VarChar, setPermissionToUser.CO_OnStrt)
    .input("CO_GAP", sql.VarChar, setPermissionToUser.CO_GAP)
    .input("CO_Accsrios", sql.VarChar, setPermissionToUser.CO_Accsrios)
    .input("CO_PrecioFlx", sql.VarChar, setPermissionToUser.CO_PrecioFlx)
    .input("CO_GarPlus", sql.VarChar, setPermissionToUser.CO_GarPlus)
    .input("CO_SegInbros", sql.VarChar, setPermissionToUser.CO_SegInbros)
    .input("CO_PrecioFlxCntdo", sql.VarChar, setPermissionToUser.CO_PrecioFlxCntdo)
    .input("DtllsCntrtsGMF", sql.VarChar, setPermissionToUser.DtllsCntrtsGMF)
    .input("CO_Resumen", sql.VarChar, setPermissionToUser.CO_Resumen)
    
    .query(querys.InsertUserFromUsuariosDMSToUsuarios);

}

const axiosUsersDMS = async() => {
  try {
    return await axios.get("http://fasaip.ddns.net:9080/globaldms/usuarios.asp")
    } catch (error) {
    console.error(error)
    }
}

const getPermisos = (listaEncapsuladaPuestos, UserDMS, UserRols) => {
  let objResult = {}
  let setPermissionToUser = {};
  let isGerente = false;
  let isAsesor = false;
  let isFYI = false;
  let isBDC = false;
  let isHostess = false;
  let responsable = 8;
  let isAuxContable = false;
  let isOtro = false;

 const [
  gerenteNames, 
  asesorVentasNames,
  FYINames,
  BDCNames,
  HostessNames,
  AuxiliarContableNames
  ] = listaEncapsuladaPuestos;

  const GerentePermissions = UserRols[0];
  const HostessPermissions = UserRols[1];
  const BDCPermissions = UserRols[2];
  const FYIPermissions = UserRols[3];
  const AuxContablePermissions = UserRols[4];
  const AsesorDeVentasPermissions = UserRols[5];
  const TIPermissions = UserRols[6];
  const OtroPermissions = UserRols[7];

/* --------------------------------------------- */

if (UserDMS.PUESTO.trim() === "" || UserDMS.PUESTO.trim() === ".") {
  isOtro = true;
  setPermissionToUser = OtroPermissions;
  responsable = setPermissionToUser.Id_responsable

} else {

  for (const iterator of gerenteNames) {
    if (UserDMS.PUESTO.trim() === iterator) {
      isGerente = true;
      setPermissionToUser = GerentePermissions;
      responsable = setPermissionToUser.Id_responsable
    }
  }

  //si no es gerente, revisamos si es asesor ventas
  if (!isGerente) {
    for (const iterator of asesorVentasNames) {
      if (UserDMS.PUESTO.trim() === iterator) {
        isAsesor = true;
        setPermissionToUser = AsesorDeVentasPermissions;
        responsable = setPermissionToUser.Id_responsable
      }
    }

  }

  if (!isGerente && !isAsesor) {
    for (const iterator of FYINames) {
      if (UserDMS.PUESTO.trim() === iterator) {
        isFYI = true;
        setPermissionToUser = FYIPermissions;
        responsable = setPermissionToUser.Id_responsable
      }
    }
  }

  if (!isGerente && !isAsesor && !isFYI) {
    for (const iterator of BDCNames) {
      if (UserDMS.PUESTO.trim() === iterator) {
        isBDC = true;
        setPermissionToUser = BDCPermissions;
        responsable = setPermissionToUser.Id_responsable
      }
    }
  }

  if (!isGerente && !isAsesor && !isFYI && !isBDC) {
    for (const iterator of HostessNames) {
      if (UserDMS.PUESTO.trim() === iterator) {
        isHostess = true;
        setPermissionToUser = HostessPermissions;
        responsable = setPermissionToUser.Id_responsable
      }
    }
  }

  if (!isGerente && !isAsesor && !isFYI && !isBDC && !isHostess) {
    for (const iterator of AuxiliarContableNames) {
      if (UserDMS.PUESTO.trim() === iterator) {
        isAuxContable = true;
        setPermissionToUser = AuxContablePermissions;
        responsable = setPermissionToUser.Id_responsable
      }

    }
  }

}

//validamos si el obj es vacío y asignamos los permisos de [otros].
if (Object.keys(setPermissionToUser).length === 0) {
  setPermissionToUser = OtroPermissions;
}

objResult = { 
  setPermissionToUser: setPermissionToUser,
  responsable: responsable
}

return objResult;

}


import { getConnection, querys, sql } from "../database"

export const getSegurosInbros = async (req, res) => {
    const { Empresa, Sucursal } = req.body;

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getCtrlOper_SegurosInbros)

        res.json(result.recordset)

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

export const createSegurosInbros = async (req, res) => {
    const { SegurosInbros, agencia } = req.body
    let IsRegisterInBD = false;

    /* try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, SegurosInbros.Fecha)
            .query(querys.ExistsRegister_SegurosInbros)

        if (result.rowsAffected > 0) IsRegisterInBD = true;
        if (result.rowsAffected < 1) { */

            try {
                const pool = await getConnection();
                insertRegisterToBD(pool, SegurosInbros, agencia);
                if (!IsRegisterInBD) res.json({ isCreated: true })

            } catch (error) {
                res.status(500);
                res.send(error.message);
            }

        /* }

        if (IsRegisterInBD) res.json({ isCreated: false })
        if (!IsRegisterInBD) res.json({ isCreated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    } */

}

const insertRegisterToBD = async (pool, SegurosInbros, agencia) => {
    await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, SegurosInbros.Fecha)
            .input("Importe_Inbros", sql.Float, SegurosInbros.Importe_Inbros)
            .input("Asesor", sql.VarChar, SegurosInbros.Asesor)
            .input("Utilidad", sql.Float, SegurosInbros.Utilidad)
            .input("Comision_Asesor", sql.Float, SegurosInbros.Comision_Asesor)
            .input("Utilidad_Neta", sql.Float, SegurosInbros.Utilidad_Neta)

            .query(querys.createCtrlOper_SegurosInbros)
}

export const updateSegurosInbros = async (req, res) => {
    const { SegurosInbros } = req.body

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Fecha", sql.VarChar, SegurosInbros.Fecha)
            .input("Importe_Inbros", sql.Float, SegurosInbros.Importe_Inbros)
            .input("Asesor", sql.VarChar, SegurosInbros.Asesor)
            .input("Nombre_Cliente", sql.VarChar, SegurosInbros.Nombre_Cliente)
            .input("Utilidad", sql.Float, SegurosInbros.Utilidad)
            .input("Comision_Asesor", sql.Float, SegurosInbros.Comision_Asesor)
            .input("Utilidad_Neta", sql.Float, SegurosInbros.Utilidad_Neta)
            .input("Id", sql.Int, SegurosInbros.Id)

            .query(querys.updateCtrlOper_SegurosInbros)

        res.json({ isUpdated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

}
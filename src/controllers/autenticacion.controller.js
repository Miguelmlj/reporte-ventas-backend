import { getConnection, querys, sql } from "../database";

export const login = async (req, res) => {
    //recordar recibir el nombre con minusculas
    const { nombre, clave, Empresa, Sucursal } = req.body;

    try {
        const pool = await getConnection();
        const result = await pool
            .request()
            .input("nombre", sql.VarChar, nombre)
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.login);

        if (result.recordset.length > 0) {

            res.json({
                nombre_usuario: result.recordset[0].nombre_usuario,
                isUserFound: true,
                empresa: result.recordset[0].Empresa,
                sucursal: result.recordset[0].Sucursal,
                tipo_usuario: result.recordset[0].tipo_usuario,
                responsable: result.recordset[0].responsable,
                inicio: result.recordset[0].inicio,
                afluencia: result.recordset[0].afluencia,
                citas: result.recordset[0].citas,
                solicitudes: result.recordset[0].solicitudes,
                anticipos: result.recordset[0].anticipos,
                invfisico: result.recordset[0].invfisico,
                codigosqr: result.recordset[0].codigosQr,
                usuarios: result.recordset[0].usuarios,
                indFYI: result.recordset[0].indFYI,
                indLeads: result.recordset[0].indLeads,
                indCentroProp: result.recordset[0].indCentroProp,
                indJDPower: result.recordset[0].indJDPower,
                indBDC: result.recordset[0].indBDC,
                indMkt: result.recordset[0].indMkt,
                CO_SegGMF: result.recordset[0].CO_SegGMF,
                CO_GarGMPlus: result.recordset[0].CO_GarGMPlus,
                CO_OnStrt: result.recordset[0].CO_OnStrt,
                CO_GAP: result.recordset[0].CO_GAP,
                CO_Accsrios: result.recordset[0].CO_Accsrios,
                CO_PrecioFlx: result.recordset[0].CO_PrecioFlx,
                CO_GarPlus: result.recordset[0].CO_GarPlus,
                CO_SegInbros: result.recordset[0].CO_SegInbros,
                CO_PrecioFlxCntdo: result.recordset[0].CO_PrecioFlxCntdo,
                DtllsCntrtsGMF: result.recordset[0].DtllsCntrtsGMF,
                CO_Resumen: result.recordset[0].CO_Resumen,
                Asig_Contratos: result.recordset[0].Asig_Contratos,
                Clientes_Flotillas: result.recordset[0].Clientes_Flotillas

            })

            /* if (clave.toUpperCase().trim() === result.recordset[0].clave.toUpperCase().trim()) {
            } else {
                res.json({ isUserFound: false })

            } */

        } else {
            res.json({ isUserFound: false })
        }

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }


}

export const loginDMSUsers = async (req, res) => {
    const { nombre, clave, Empresa, Sucursal } = req.body;


    try {
        const pool = await getConnection();
        const result = await pool
            .request()
            .input("nombre", sql.VarChar, nombre)
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.loginDMS);

        if (result.recordset.length > 0) {
            if (clave.toUpperCase().trim() === result.recordset[0].Clave.toUpperCase().trim()) {
                res.json({
                    Usuario: result.recordset[0].Usuario,
                    Nombre: result.recordset[0].Nombre,
                    Clave: result.recordset[0].Clave,
                    isUserFound: true
                })

            } else {
                res.json({ isUserFound: false })
            }

        } else {
            res.json({ isUserFound: false })
        }


    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}
import { getConnection, querys, sql } from "../database"

export const getGarantiasGMPlus = async (req, res) => {
    const { Empresa, Sucursal } = req.body;
    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getGarantiasGMPlus)

        res.json(result.recordset)

    } catch (error) {
        res.status(500);
        res.send(error.message); 
    }
}

export const createGarantiasGMPlus = async (req, res) => {
    const { garantiasGMPlus, agencia } = req.body

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, garantiasGMPlus.Fecha)
            .input("Importe_Garantia", sql.Float, garantiasGMPlus.Importe_Garantia)
            .input("Asesor", sql.VarChar, garantiasGMPlus.Asesor)
            .input("Nombre_Cliente", sql.VarChar, garantiasGMPlus.Nombre_Cliente)
            .input("Utilidad", sql.Float, garantiasGMPlus.Utilidad)
            .input("Comision_Asesor", sql.Float, garantiasGMPlus.Comision_Asesor)
            .input("Utilidad_Neta", sql.Float, garantiasGMPlus.Utilidad_Neta)

            .query(querys.createGarantiasGMPlus)

        res.json({ isCreated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message); 
    }

}

export const updateGarantiasGMPlus = async (req, res) => {
    const { garantiasGMPlus } = req.body

    try {
        const pool = await getConnection();
        await pool.request()
        .input("Fecha", sql.VarChar, garantiasGMPlus.Fecha)
        .input("Importe_Garantia", sql.Float, garantiasGMPlus.Importe_Garantia)
        .input("Asesor", sql.VarChar, garantiasGMPlus.Asesor)
        .input("Nombre_Cliente", sql.VarChar, garantiasGMPlus.Nombre_Cliente)
        .input("Utilidad", sql.Float, garantiasGMPlus.Utilidad)
        .input("Comision_Asesor", sql.Float, garantiasGMPlus.Comision_Asesor)
        .input("Utilidad_Neta", sql.Float, garantiasGMPlus.Utilidad_Neta)
        .input("Id", sql.Int, garantiasGMPlus.Id)

        .query(querys.updateGarantiasGMPlus)

        res.json({ isUpdated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message); 
    }
}
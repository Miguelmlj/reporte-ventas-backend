import { getConnection, querys, sql } from "../database"

export const getIndicadorMkt = async (req, res) => {
    const { Empresa, Sucursal } = req.body;

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getIndicadorMkt)

        res.json(result.recordset);
    } catch (error) {
        res.status(500);
        res.send(error.message)
    }
}

export const createIndicadorMkt = async (req, res) => {
    const { agencia, indicadorMkt } = req.body;

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, indicadorMkt.Fecha)
            .input("Lead_GrupoFelix", sql.Int, indicadorMkt.Lead_GrupoFelix)
            .input("Lead_PaginaLocal", sql.Int, indicadorMkt.Lead_PaginaLocal)

            .query(querys.createIndicadorMkt)

        res.json({ isCreated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

export const updateIndicadorMkt = async (req, res) => {
    const { indicadorMkt } = req.body;

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Fecha", sql.VarChar, indicadorMkt.Fecha)
            .input("Lead_GrupoFelix", sql.Int, indicadorMkt.Lead_GrupoFelix)
            .input("Lead_PaginaLocal", sql.Int, indicadorMkt.Lead_PaginaLocal)
            .input("Id", sql.Int, indicadorMkt.Id)
            .query(querys.updateIndicadorMkt)

            res.json( { isUpdated: true } )

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

/* createIndicadorMkt
updateIndicadorMkt */
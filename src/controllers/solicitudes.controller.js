import { getConnection, querys, sql } from "../database";

export const getSolicitudes = async (req, res) => {
  const { empresa, sucursal } = req.body;
  let cuery = querys.getAllSolicitudes;

    try {
      const pool = await getConnection();   
      const result = await pool.request()
      .input("empresa", sql.Int, empresa)
      .input("sucursal", sql.Int, sucursal)
      .query(cuery);
      res.json(result.recordset); 
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

//petición de fecha. El cliente decide si crear o actualizar
export const getSolicitudesByFecha = async (req, res) => {
    const { id_fecha, empresa, sucursal } = req.body;

    let cuery = querys.getSolicitudesByFecha;

    try {
        const pool = await getConnection();

        const result = await pool
          .request()

          .input("id_fecha", sql.VarChar, id_fecha)
          .input("empresa", sql.Int, empresa)
          .input("sucursal", sql.Int, sucursal)
          .query(cuery);
    
        if (result.rowsAffected > 0) {
          res.json({ message: "registrocreado" });
        } else {
          res.json({ message: "registronocreado" });
    
        } 
        
    } catch (error) {
        res.status(500);
        res.send(error.message);  
    }

}

export const createSolicitudes = async (req, res) => {
    const {
        id_fecha,
        sol_gmf_n,
        sol_bancos_n,
        sol_aprobaciones_gmf_n,
        sol_contratos_comprados_n,
        sol_contratos_comprados_leasing,
    
        sol_gmf_s,
        sol_bancos_s,
        sol_aprobaciones_gmf_s,
        sol_contratos_comprados_s,
        
        empresa,
        sucursal
      } = req.body;

      let cuery = querys.createSolicitudes;


     try {
        const pool = await getConnection();
        await pool
          .request()
          .input("empresa", sql.Int, empresa)
          .input("sucursal", sql.Int, sucursal)
          .input("id_fecha", sql.Date, id_fecha)
          .input("sol_gmf_n", sql.Int, sol_gmf_n)
          .input("sol_bancos_n", sql.Int, sol_bancos_n)
          .input("sol_aprobaciones_gmf_n", sql.Int, sol_aprobaciones_gmf_n)
          .input("sol_contratos_comprados_n", sql.Int, sol_contratos_comprados_n)
          .input("sol_contratos_comprados_leasing", sql.Int, sol_contratos_comprados_leasing)
    
          .input("sol_gmf_s", sql.Int, sol_gmf_s)
          .input("sol_bancos_s", sql.Int, sol_bancos_s)
          .input("sol_aprobaciones_gmf_s", sql.Int, sol_aprobaciones_gmf_s)
          .input("sol_contratos_comprados_s", sql.Int, sol_contratos_comprados_s)
    
          .query(cuery);
    
        res.json({
          id_fecha,
          sol_gmf_n,
          sol_bancos_n,
          sol_aprobaciones_gmf_n,
          sol_contratos_comprados_n,
          sol_contratos_comprados_leasing,
    
          sol_gmf_s,
          sol_bancos_s,
          sol_aprobaciones_gmf_s,
          sol_contratos_comprados_s,
    
        });
         
     } catch (error) {
        res.status(500);
        res.send(error.message);
         
     }
  
}

export const updateSolicitudesByFecha = async (req, res) => {
    const {
        id_fecha,
        sol_gmf_n,
        sol_bancos_n,
        sol_aprobaciones_gmf_n,
        sol_contratos_comprados_n,
        sol_contratos_comprados_leasing,
    
        sol_gmf_s,
        sol_bancos_s,
        sol_aprobaciones_gmf_s,
        sol_contratos_comprados_s,
        
        empresa,
        sucursal
      } = req.body;

      let cuery = querys.updateSolicitudesByFecha;

     try {
        const pool = await getConnection();
        await pool
          .request()
          .input("id_fecha", sql.Date, id_fecha)
          .input("sol_gmf_n", sql.Int, sol_gmf_n)
          .input("sol_bancos_n", sql.Int, sol_bancos_n)
          .input("sol_aprobaciones_gmf_n", sql.Int, sol_aprobaciones_gmf_n)
          .input("sol_contratos_comprados_n", sql.Int, sol_contratos_comprados_n)
          .input("sol_contratos_comprados_leasing", sql.Int, sol_contratos_comprados_leasing)
    
          .input("sol_gmf_s", sql.Int, sol_gmf_s)
          .input("sol_bancos_s", sql.Int, sol_bancos_s)
          .input("sol_aprobaciones_gmf_s", sql.Int, sol_aprobaciones_gmf_s)
          .input("sol_contratos_comprados_s", sql.Int, sol_contratos_comprados_s)
          .input("empresa", sql.Int, empresa)
          .input("sucursal", sql.Int, sucursal)
    
          .query(cuery);
    
        res.json({
          id_fecha,
          sol_gmf_n,
          sol_bancos_n,
          sol_aprobaciones_gmf_n,
          sol_contratos_comprados_n,
          sol_contratos_comprados_leasing,
    
          sol_gmf_s,
          sol_bancos_s,
          sol_aprobaciones_gmf_s,
          sol_contratos_comprados_s,
    
        });
         
     } catch (error) {
        res.status(500);
        res.send(error.message);
     }

}
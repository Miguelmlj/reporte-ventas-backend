import { getConnection, querys, sql } from "../database";

//obtener todas las citas
export const getCitas = async (req, res) => {
  const { empresa, sucursal } = req.body;
  let cuery = querys.getAllCitas;


  try {
    const pool = await getConnection();
    const result = await pool.request()
    .input("empresa", sql.Int, empresa)
    .input("sucursal", sql.Int, sucursal)
    .query(cuery);
    res.json(result.recordset);
  } catch (error) {
    res.status(500);
    res.send(error.message);
  }
}

//obtener cita por fecha / comprobar si existo o no un registro con la fecha
//Para saber el cliente si actualiza o crea el registro.
export const getCitasByFecha = async (req, res) => {
  const { id_fecha, empresa, sucursal } = req.body;

  let cuery = querys.getCitasByFecha;


  try {
    const pool = await getConnection();

    const result = await pool
      .request()

      .input("id_fecha", sql.VarChar, id_fecha)
      .input("empresa", sql.Int, empresa)
      .input("sucursal", sql.Int, sucursal)
      .query(cuery);
      // .query(querys.getCitasByFecha);

    if (result.rowsAffected > 0) {
      res.json({ message: "registrocreado" });
    } else {
      res.json({ message: "registronocreado" });

    }

  } catch (error) {
    res.status(500);
    res.send(error.message);
  }

}

//crear la cita[campos de cita] (reporte diario) cuando aún no haya sido creada por los otros usuarios no exista
export const createCitas = async (req, res) => {

  const {
    id_fecha,
    cit_agendadas_n,
    cit_cumplidas_n,
    empresa,
    sucursal
  } = req.body;

  let cuery = querys.createCitas;


  try {
    const pool = await getConnection();
    await pool
      .request()
      .input("empresa", sql.Int, empresa)
      .input("sucursal", sql.Int, sucursal)
      .input("id_fecha", sql.Date, id_fecha)
      .input("cit_agendadas_n", sql.Int, cit_agendadas_n)
      .input("cit_cumplidas_n", sql.Int, cit_cumplidas_n)
      .query(cuery);

    res.json({
      id_fecha,
      cit_agendadas_n,
      cit_cumplidas_n
      
    });
  } catch (error) {
    res.status(500);
    res.send(error.message);
  }

}

//actualizar registro por fecha, se utiliza un patch para hacer cambios parciales en la tabla, campos de cita específicos
export const updateCitasByFecha = async (req, res) => {
  const { 
    id_fecha, 
    cit_agendadas_n, 
    cit_cumplidas_n, 
    empresa,
    sucursal
   } = req.body;

   let cuery = querys.updateCitasByFecha;


  try {
    const pool = await getConnection();
    await pool
      .request()
      .input("cit_agendadas_n", sql.Int, cit_agendadas_n)
      .input("cit_cumplidas_n", sql.Int, cit_cumplidas_n)
      .input("id_fecha", sql.VarChar, id_fecha)
      .input("empresa", sql.Int, empresa)
      .input("sucursal", sql.Int, sucursal)

      .query(cuery);

    // console.log(result);

    res.json({ 
      id_fecha, 
      cit_agendadas_n, 
      cit_cumplidas_n 
      /* cit_primera_cita_n, 
      cit_digitales_n  */
    });

  } catch (error) {
    res.status(500);
    res.send(error.message);
  }

}


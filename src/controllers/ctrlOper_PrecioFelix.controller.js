import { getConnection, querys, sql } from "../database"

export const getPrecioFelix = async (req, res) => {
    const { Empresa, Sucursal } = req.body;

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getCtrlOper_PrecioFelix)

        res.json(result.recordset)

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

}

export const createPrecioFelix = async (req, res) => {
    const { PrecioFelix, agencia } = req.body
    let IsRegisterInBD = false;

    /* try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, PrecioFelix.Fecha)
            .query(querys.ExistsRegister_PrecioFelix)

        if (result.rowsAffected > 0) IsRegisterInBD = true;

        if (result.rowsAffected < 1) { */

            try {
                const pool = await getConnection();
                insertRegisterToBD(pool, PrecioFelix, agencia);
                if (!IsRegisterInBD) res.json({ isCreated: true })

            } catch (error) {
                res.status(500);
                res.send(error.message);
            }

        /* }

        if (IsRegisterInBD) res.json({ isCreated: false })
        if (!IsRegisterInBD) res.json({ isCreated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    } */

}

const insertRegisterToBD = async (pool, PrecioFelix, agencia) => {
    await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, PrecioFelix.Fecha)
            .input("Importe_PrecioFelix", sql.Float, PrecioFelix.Importe_PrecioFelix)
            .input("Asesor", sql.VarChar, PrecioFelix.Asesor)
            .input("Nombre_Cliente", sql.VarChar, PrecioFelix.Nombre_Cliente)
            .input("Factura_PrecioFelix", sql.VarChar, PrecioFelix.Factura_PrecioFelix)
            .input("Utilidad", sql.Float, PrecioFelix.Utilidad)
            .input("Comision_Asesor", sql.Float, PrecioFelix.Comision_Asesor)
            .input("Utilidad_Neta", sql.Float, PrecioFelix.Utilidad_Neta)

            .query(querys.createCtrlOper_PrecioFelix)

}

export const updatePrecioFelix = async (req, res) => {
    const { PrecioFelix } = req.body

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Fecha", sql.VarChar, PrecioFelix.Fecha)
            .input("Importe_PrecioFelix", sql.Float, PrecioFelix.Importe_PrecioFelix)
            .input("Asesor", sql.VarChar, PrecioFelix.Asesor)
            .input("Nombre_Cliente", sql.VarChar, PrecioFelix.Nombre_Cliente)
            .input("Factura_PrecioFelix", sql.VarChar, PrecioFelix.Factura_PrecioFelix)
            .input("Utilidad", sql.Float, PrecioFelix.Utilidad)
            .input("Comision_Asesor", sql.Float, PrecioFelix.Comision_Asesor)
            .input("Utilidad_Neta", sql.Float, PrecioFelix.Utilidad_Neta)
            .input("Id", sql.Int, PrecioFelix.Id)

            .query(querys.updateCtrlOper_PrecioFelix)

        res.json({ isUpdated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}
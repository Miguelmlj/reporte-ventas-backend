
import { getConnection, querys, sql } from "../database";

export const getAnticipos = async (req, res) => {
    const { Empresa, Sucursal, nombreAsesor, rol } = req.body;
    let cuery;
    //si el Rol es TI
    if (rol === "7") {
        cuery = querys.getAllAnticipos //rol = TI = 7, en BD  
        try {
            const pool = await getConnection();
            const result = await pool.request()
                .input("Empresa", sql.Int, Empresa)
                .input("Sucursal", sql.Int, Sucursal)
                // .input("Nombre_asesor", sql.VarChar, nombreAsesor)
                .query(cuery);

            res.json(result.recordset);
        } catch (error) {
            res.status(500);
            res.send(error.message);
        }

    } else if(rol === "1"){
        cuery = querys.getAnticiposGerente;
        try {
            const pool = await getConnection();
            const result = await pool.request()
                .input("Empresa", sql.Int, Empresa)
                .input("Sucursal", sql.Int, Sucursal)
                .query(cuery);

            res.json(result.recordset);
        } catch (error) {
            res.status(500);
            res.send(error.message);
        }

    } else {
        cuery = querys.getAnticiposOfAsesor;
        try {
            const pool = await getConnection();
            const result = await pool.request()
                .input("Empresa", sql.Int, Empresa)
                .input("Sucursal", sql.Int, Sucursal)
                .input("Nombre_asesor", sql.VarChar, nombreAsesor)
                .query(cuery);

            res.json(result.recordset);
        } catch (error) {
            res.status(500);
            res.send(error.message);
        }
    }

}

export const getAnticipoById = async (req, res) => {
    const { id } = req.params;
    let cuery = querys.getAnticipoById;

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Id_anticipo", sql.Int, id)
            .query(cuery)

        res.json(result.recordset)
    } catch (error) {
        res.status(500);
        res.send(error.message)
    }
}

export const updateAnticipo = async (req, res) => {
    const { id } = req.params;
    const {
        Empresa,
        Sucursal,
        Folio_anticipo,
        Fecha, 
        Nombre_cliente,
        Unidad,
        Paquete,
        Color,
        Tipo_compra,
        Nombre_asesor,
        Agencia
    } = req.body;

    let cuery = querys.updateAnticipo;

    try {
        const pool = await getConnection();
        await pool
            .request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .input("Folio_anticipo", sql.VarChar, Folio_anticipo)
            .input("Fecha", sql.Date, Fecha)
            .input("Nombre_cliente", sql.VarChar, Nombre_cliente)
            .input("Unidad", sql.VarChar, Unidad)
            .input("Paquete", sql.VarChar, Paquete)
            .input("Color", sql.VarChar, Color)
            .input("Tipo_compra", sql.VarChar, Tipo_compra)
            .input("Nombre_asesor", sql.VarChar, Nombre_asesor)
            .input("Agencia", sql.VarChar, Agencia)
            .input("Id_anticipo", sql.Int, id)

            .query(cuery);

        res.json({
            message: "El anticipo fue actualizado con éxito."
        })

    } catch (error) {
        res.status(500);
        res.send(error.message);
        console.log(error.message);
    }

}

export const createAnticipo = async (req, res) => {
    const {
        Empresa,
        Sucursal,
        Folio_anticipo,
        Fecha,
        Nombre_cliente,
        Unidad,
        Paquete,
        Color,
        Tipo_compra,
        Nombre_asesor,
        Agencia
    } = req.body;
    
    let cuery = querys.createAnticipo;

    try {
        const pool = await getConnection();
        await pool
            .request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .input("Folio_anticipo", sql.VarChar, Folio_anticipo)
            .input("Fecha", sql.Date, Fecha)
            .input("Nombre_cliente", sql.VarChar, Nombre_cliente)
            .input("Unidad", sql.VarChar, Unidad)
            .input("Paquete", sql.VarChar, Paquete)
            .input("Color", sql.VarChar, Color)
            .input("Tipo_compra", sql.VarChar, Tipo_compra)
            .input("Nombre_asesor", sql.VarChar, Nombre_asesor)
            .input("Agencia", sql.VarChar, Agencia)

            .query(cuery);

        res.json({
            message: "El anticipo fue registrado con éxito."
        })

    } catch (error) {
        res.status(500);
        res.send(error.message);
        console.log(error.message);
    }

}
import { getConnection, querys, sql } from "../database";
import { FechaDeHoy } from "../database";

export const tableVehiculosIsEmpty = async (req, res) => {

    try {
        const pool = await getConnection();
        const result = await pool
            .request()
            .query(querys.ExistsVehiculosSQL)

        if (result.rowsAffected > 0) {
            res.json({ isEmpty: false });

        } else {
            res.json({ isEmpty: true });

        }

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

}

export const insertAllRowsOfDMSToVehiculos = async (req, res) => {
    const requestServer = await axiosVehiculosDMS();
    let impreso = "N";
    
    try {
        const pool = await getConnection();
        for (const vehi_dms of requestServer) {
            await pool
                .request()
                .input("Empresa", sql.Int, vehi_dms.Empresa)
                .input("Sucursal", sql.Int, vehi_dms.Sucursal)
                .input("Inventario", sql.VarChar, vehi_dms.Inventario)
                .input("mode_clave", sql.VarChar, vehi_dms.mode_clave)
                .input("mode_tipo", sql.VarChar, vehi_dms.mode_tipo)
                .input("mode_descripcion", sql.VarChar, vehi_dms.mode_descripcion)
                .input("vehi_serie", sql.VarChar, vehi_dms.vehi_serie)
                .input("color", sql.VarChar, vehi_dms.color)
                .input("marc_descrip", sql.VarChar, vehi_dms.marc_descrip)
                .input("vehi_anio_modelo", sql.VarChar, vehi_dms.vehi_anio_modelo)
                .input("Vehi_clase", sql.VarChar, vehi_dms.Vehi_clase)
                .input("impreso", sql.VarChar, impreso)
                .query(querys.insertVehi_DMS)

        }

        res.json({ isCreated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
        console.log(error.message);
        console.log(error);
    }

}

export const insertVehiculosSQL = async (req, res) => {
    const carList = req.body;
    let impreso = "N";

    try {
        const pool = await getConnection();
        for (const vehi_dms of carList) {
            await pool
                .request()
                .input("Empresa", sql.Int, vehi_dms.Empresa)
                .input("Sucursal", sql.Int, vehi_dms.Sucursal)
                .input("Inventario", sql.VarChar, vehi_dms.Inventario)
                .input("mode_clave", sql.VarChar, vehi_dms.mode_clave)
                .input("mode_tipo", sql.VarChar, vehi_dms.mode_tipo)
                .input("mode_descripcion", sql.VarChar, vehi_dms.mode_descripcion)
                .input("vehi_serie", sql.VarChar, vehi_dms.vehi_serie)
                .input("color", sql.VarChar, vehi_dms.color)
                .input("marc_descrip", sql.VarChar, vehi_dms.marc_descrip)
                .input("vehi_anio_modelo", sql.VarChar, vehi_dms.vehi_anio_modelo)
                .input("Vehi_clase", sql.VarChar, vehi_dms.Vehi_clase)
                .input("impreso", sql.VarChar, impreso)
                .query(querys.insertVehi_DMS)

        }

        res.json({ isCreated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);

    }
}

export const deleteVehiculosSQL = async (req, res) => {
    const carList = req.body;
    
    try {

        const pool = await getConnection();

        for (const car of carList) {
            await pool.request()
                .input("vehi_serie", sql.VarChar, car.vehi_serie)
                .query(querys.deleteVehiculosSQL)

        }

        res.json({ isDeleted: true });

    } catch (error) {
        res.status(500);
        res.send(error.message);
        console.log(error.message);
        console.log(error);
    }

}

export const getVehiculosDMS = async (req, res) => {
    try {

        const pool = await getConnection();
        const result = await
            pool.request()
                .query(querys.getVehiculosDMS)

        res.json(result.recordset);
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

export const getVehiculosSQL = async (req, res) => {
    try {

        const pool = await getConnection();
        const result = await
            pool.request()
                .query(querys.getVehiculosSQL)

        res.json(result.recordset);
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

export const updateVehiculosSQL = async (req, res) => {
    const vinList = req.body;

    try {
        const pool = await getConnection();

        for (const register of vinList) {
            const result = await
                pool.request()
                    .input("Empresa", sql.Int, register.Empresa)
                    .input("Sucursal", sql.Int, register.Sucursal)
                    .input("vehi_serie", sql.VarChar, register.vehi_serie)
                    .query(querys.VINExistsInTable)

            if (result.rowsAffected > 0) {
                //si existe el registro, no haremos nada.
            } else {
                //si no existe el registro, lo insertaremos.
                try {
                    insertNewVin(register,pool)

                } catch (error) {
                    res.status(500);
                    res.send(error.message);
                }
            }
        }

        res.json({ message: "ok" })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

}

const insertNewVin = async (register, pool) => {
    let impreso = "S";
    let Fecha = FechaDeHoy();

    await pool
        .request()
        .input("Empresa", sql.Int, register.Empresa)
        .input("Sucursal", sql.Int, register.Sucursal)
        .input("vehi_serie", sql.VarChar, register.vehi_serie)
        .input("impreso", sql.VarChar, impreso)
        .input("Fecha", sql.VarChar, Fecha)

        .query(querys.InsertVINToVehiculos)

}

const axiosVehiculosDMS = async () => {

    try {

        const pool = await getConnection();
        const result = await
            pool.request()
                .query(querys.getVehiculosDMS)

        return result.recordset;
    } catch (error) {
        console.error(error)
    }
}
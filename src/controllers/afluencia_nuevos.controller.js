import { getConnection, querys, sql } from "../database";

export const getAfluenciaNuevos = async (req, res) => { //este nombre de arrow function es temporal
  try {
    const pool = await getConnection();
    const result = await pool.request().query(querys.getReporteDiario);
    res.json(result.recordset);
  } catch (error) {
    res.status(500);
    res.send(error.message);
  }
}

//este igual, temporal
export const createAfluenciaNuevos = async (req, res) => {

  const {
    // id_reporte,
    id_fecha,
    af_fresh_n,
    af_bback_n,
    af_primera_cita_n,
    af_digitales_n,
    sol_gmf_n,
    sol_bancos_n,
    sol_aprobaciones_gmf_n,
    sol_contratos_comprados_n,
    af_fresh_s,
    af_bback_s,
    af_primera_cita_s,
    af_digitales_s,
    sol_gmf_s,
    sol_bancos_s,
    sol_aprobaciones_gmf_s,
    sol_contratos_comprados_s,
    cit_agendadas_n,
    cit_cumplidas_n,
    cit_primera_cita_n,
    cit_digitales_n
  } = req.body;

  // validating
  if (
    // id_reporte                == null ||
    id_fecha                  == '' ||
    af_fresh_n                == -1 ||
    af_bback_n                == -1 ||
    af_primera_cita_n         == -1 ||
    af_digitales_n            == -1 ||
    
    sol_gmf_n                 == -1 ||
    sol_bancos_n              == -1 ||
    sol_aprobaciones_gmf_n    == -1 ||
    sol_contratos_comprados_n == -1 ||
   
    af_fresh_s                == -1 ||
    af_bback_s                == -1 ||
    af_primera_cita_s         == -1 ||
    af_digitales_s            == -1 ||
    
    sol_gmf_s                 == -1 ||
    sol_bancos_s              == -1 ||
    sol_aprobaciones_gmf_s    == -1 ||
    sol_contratos_comprados_s == -1 ||
    
    cit_agendadas_n           == -1 ||
    cit_cumplidas_n           == -1 ||
    cit_primera_cita_n        == -1 ||
    cit_digitales_n           == -1 
    ) {
    return res.status(400).json({ msg: "Registro incorrecto. Por favor, llene todos los campos" });
  }

  // res.json({"msg": 'paso validacion'})


  try {
    const pool = await getConnection();

    await pool
      .request()
      // .input("id_reporte", sql.Int, id_reporte)
      .input("id_fecha", sql.Date, id_fecha)
      .input("af_fresh_n", sql.Int, af_fresh_n)
      .input("af_bback_n", sql.Int, af_bback_n)
      .input("af_primera_cita_n", sql.Int, af_primera_cita_n)
      .input("af_digitales_n", sql.Int, af_digitales_n)

      .input("sol_gmf_n", sql.Int, sol_gmf_n)
      .input("sol_bancos_n", sql.Int, sol_bancos_n)
      .input("sol_aprobaciones_gmf_n", sql.Int, sol_aprobaciones_gmf_n)
      .input("sol_contratos_comprados_n", sql.Int, sol_contratos_comprados_n)

      .input("af_fresh_s", sql.Int, af_fresh_s)
      .input("af_bback_s", sql.Int, af_bback_s)
      .input("af_primera_cita_s", sql.Int, af_primera_cita_s)
      .input("af_digitales_s", sql.Int, af_digitales_s)

      .input("sol_gmf_s", sql.Int, sol_gmf_s)
      .input("sol_bancos_s", sql.Int, sol_bancos_s)
      .input("sol_aprobaciones_gmf_s", sql.Int, sol_aprobaciones_gmf_s)
      .input("sol_contratos_comprados_s", sql.Int, sol_contratos_comprados_s)
      
      .input("cit_agendadas_n", sql.Int, cit_agendadas_n)
      .input("cit_cumplidas_n", sql.Int, cit_cumplidas_n)
      .input("cit_primera_cita_n", sql.Int, cit_primera_cita_n)
      .input("cit_digitales_n", sql.Int, cit_digitales_n)

      .query(querys.addReporteDiarioTodos);

    res.json({ 
      // id_reporte,
      id_fecha, 
      af_fresh_n, 
      af_bback_n,
      af_primera_cita_n,
      af_digitales_n,
      
      sol_gmf_n,
      sol_bancos_n,
      sol_aprobaciones_gmf_n,
      sol_contratos_comprados_n,

      af_fresh_s,
      af_bback_s,
      af_primera_cita_s,
      af_digitales_s,
      
      sol_gmf_s,
      sol_bancos_s,
      sol_aprobaciones_gmf_s,
      sol_contratos_comprados_s,

      cit_agendadas_n,
      cit_cumplidas_n,
      cit_primera_cita_n,
      cit_digitales_n
    });

  } catch (error) {
    res.status(500);
    res.send(error.message);
  }

  
};
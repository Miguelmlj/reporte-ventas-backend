import { getConnection, querys, sql } from "../database";

export const createIndicadorBDC = async ( req, res ) => {
    const { agencia, indicadorBDC } = req.body;

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, indicadorBDC.Fecha)
            .input("Llamadas_entrantesVehNvo", sql.Int, indicadorBDC.Llamadas_entrantesVehNvo)

            .query(querys.createIndicadorBDC)

            res.json({ isCreated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

export const updateIndicadorBDC = async ( req, res ) => {
    const { indicadorBDC  } = req.body;

    try {
        const pool = await getConnection();
        await pool.request()
            
            .input("Fecha", sql.VarChar, indicadorBDC.Fecha)
            .input("Llamadas_entrantesVehNvo", sql.Int, indicadorBDC.Llamadas_entrantesVehNvo)
            .input("Id", sql.Int, indicadorBDC.Id)
            .query(querys.updateIndicadorBDC)

            res.json({ isUpdated: true })
        
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

export const getIndicadorBDC = async ( req, res ) => {
    const { Empresa, Sucursal } = req.body;

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getIndicadorBDC)

            res.json( result.recordset )
        
    } catch (error) {
        res.status(500);
        res.send(error.message)
    }
}
import { getConnection, querys, sql } from "../database"

export const getPrecioFelixContado = async (req, res) => {
    const { Empresa, Sucursal } = req.body;

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getCtrlOper_PrecioFelixContado)

        res.json(result.recordset)

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

}

export const createPrecioFelixContado = async (req, res) => {
    const { PrecioFelixContado, agencia } = req.body 
    let IsRegisterInBD = false;

    /* try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, PrecioFelixContado.Fecha)
            .query(querys.ExistsRegister_PrecioFelixContado)

        if (result.rowsAffected > 0) IsRegisterInBD = true;
        if (result.rowsAffected < 1) { */

            try {
                const pool = await getConnection();
               insertRegisterToBD(pool, PrecioFelixContado, agencia);
               if (!IsRegisterInBD) res.json({ isCreated: true })

            } catch (error) {
                res.status(500);
                res.send(error.message);
            }

        /* }

        if (IsRegisterInBD) res.json({ isCreated: false })
        if (!IsRegisterInBD) res.json({ isCreated: true })
        
    } catch (error) {
        res.status(500);
        res.send(error.message);
    } */

}

const insertRegisterToBD = async (pool, PrecioFelixContado, agencia) => {
    await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, PrecioFelixContado.Fecha)
            .input("Importe_PrecioFelixContado", sql.Float, PrecioFelixContado.Importe_PrecioFelixContado)
            .input("Asesor", sql.VarChar, PrecioFelixContado.Asesor)
            .input("Comision_Asesor", sql.Float, PrecioFelixContado.Comision_Asesor)

            .query(querys.createCtrlOper_PrecioFelixContado)
}

export const updatePrecioFelixContado = async (req, res) => {
    const { PrecioFelixContado } = req.body 

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Fecha", sql.VarChar, PrecioFelixContado.Fecha)
            .input("Importe_PrecioFelixContado", sql.Float, PrecioFelixContado.Importe_PrecioFelixContado)
            .input("Asesor", sql.VarChar, PrecioFelixContado.Asesor)
            .input("Comision_Asesor", sql.Float, PrecioFelixContado.Comision_Asesor)
            .input("Id", sql.Int, PrecioFelixContado.Id)

            .query(querys.updateCtrlOper_PrecioFelixContado)

        res.json({ isUpdated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

}
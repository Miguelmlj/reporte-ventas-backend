import { getConnection, querys, sql } from "../database"

export const getLeadsJDPower = async ( req, res ) => {
    const { Empresa, Sucursal } = req.body;

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getLeadsJDPower)
            
            res.json( result.recordset ) 
            
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
   
    
}

export const createLeadsJDPower = async ( req, res ) => {
    const { leadsJDPower, agencia } = req.body;

    try {
        const pool = await getConnection();
        await pool.request()
            .input( "Empresa", sql.Int, agencia.Empresa )
            .input( "Sucursal", sql.Int, agencia.Sucursal )
            .input( "Fecha", sql.VarChar, leadsJDPower.Fecha )
            .input( "Cantidad_encuestas", sql.Int, leadsJDPower.Cantidad_encuestas )
        
            .query( querys.createLeadsJDPower )

            res.json( { isCreated: true } )

    } catch (error) {
        res.status(500);
        res.send(error.message);
        console.log(error.message);
    }
}

export const updateLeadsJDPower = async( req, res ) => {
    const { leadsJDPower } = req.body;

    try {
        const pool = await getConnection();
        await pool.request()
        .input("Fecha", sql.VarChar, leadsJDPower.Fecha)
        .input("Cantidad_encuestas", sql.Int, leadsJDPower.Cantidad_encuestas)
        .input("Id", sql.Int, leadsJDPower.Id)
        .query(querys.updateLeadsJDPower)

        res.json( { isUpdated: true } )

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}
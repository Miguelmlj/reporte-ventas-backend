import { getConnection, querys, sql} from "../database";

export const getAsesores = async (req, res) => {
    const { Empresa, Sucursal } = req.body;
    
    try {
        const pool = await getConnection();
        const result = await pool
            .request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getAsesores)

            res.json(result.recordset);
    } catch (error) {
        res.status(500);
        res.send(error.message);
        console.log(error.message);
    }
}

export const getAsesoresActivos = async (req, res) => {
    const { Empresa, Sucursal } = req.body;
    try {
        const pool = await getConnection();
        const result = await pool
            .request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getAsesoresActivos)

            res.json(result.recordset);
        
    } catch (error) {
        res.status(500);
        res.send(error.message);
        console.log(error.message);
    }
}
import { getConnection, querys, sql } from "../database"

export const getOnStar = async (req, res) => {
    const { Empresa, Sucursal } = req.body;
    try {

        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getCtrlOper_OnStar)

        res.json(result.recordset)

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

export const createOnStar = async (req, res) => {
    const { OnStar, agencia } = req.body
    let IsRegisterInBD = false;

    /* try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, OnStar.Fecha)
            .query(querys.ExistsRegister_OnStar)

        if (result.rowsAffected > 0) {
            IsRegisterInBD = true;
           

        } else { */
            //función para hacer la inserción del registro.
            try {
                const pool = await getConnection();
                insertRegisterToBD( pool, OnStar, agencia );

                if (!IsRegisterInBD) res.json({ isCreated: true })
                
            } catch (error) {
                res.status(500);
                res.send(error.message);
            }
       /*  }

        if (IsRegisterInBD) res.json({ isCreated: false })
        if (!IsRegisterInBD) res.json({ isCreated: true })


    } catch (error) {
        res.status(500);
        res.send(error.message);
    } */
}

const insertRegisterToBD = async ( pool, OnStar, agencia ) => {
    await pool.request()
    .input("Empresa", sql.Int, agencia.Empresa)
    .input("Sucursal", sql.Int, agencia.Sucursal)
    .input("Fecha", sql.VarChar, OnStar.Fecha)
    .input("Importe_OnStar", sql.Float, OnStar.Importe_OnStar)
    .input("Asesor", sql.VarChar, OnStar.Asesor)
    .input("Nombre_Cliente", sql.VarChar, OnStar.Nombre_Cliente)
    .input("Utilidad", sql.Float, OnStar.Utilidad)
    .input("Comision_Asesor", sql.Float, OnStar.Comision_Asesor)
    .input("Utilidad_Neta", sql.Float, OnStar.Utilidad_Neta)

    .query(querys.createCtrlOper_OnStar)
}

export const updateOnStar = async (req, res) => {
    const { OnStar } = req.body

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Fecha", sql.VarChar, OnStar.Fecha)
            .input("Importe_OnStar", sql.Float, OnStar.Importe_OnStar)
            .input("Asesor", sql.VarChar, OnStar.Asesor)
            .input("Nombre_Cliente", sql.VarChar, OnStar.Nombre_Cliente)
            .input("Utilidad", sql.Float, OnStar.Utilidad)
            .input("Comision_Asesor", sql.Float, OnStar.Comision_Asesor)
            .input("Utilidad_Neta", sql.Float, OnStar.Utilidad_Neta)
            .input("Id", sql.Int, OnStar.Id)

            .query(querys.updateCtrlOper_OnStar)

        res.json({ isUpdated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

}
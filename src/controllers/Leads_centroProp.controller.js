import { getConnection, querys, sql } from "../database"

export const getLeadsCentroProp = async (req, res) => {
    const { Empresa, Sucursal } = req.body;
    
    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getLeadsCentroProp)

        res.json(result.recordset)

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

export const createLeadsCentroProp = async (req, res) => {
    const { leadsCentroProp, agencia } = req.body

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, leadsCentroProp.Fecha)
            .input("Encuesta_pulso", sql.Int, leadsCentroProp.Encuesta_pulso)

            .query(querys.createLeadsCentroProp)

        res.json({ isCreated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

export const updateLeadsCentroProp = async (req, res) => {
    const { leadsCentroProp } = req.body;

    try {
        const pool = await getConnection();
        await pool.request()
        .input("Fecha", sql.VarChar, leadsCentroProp.Fecha)
        .input("Encuesta_pulso",sql.Int, leadsCentroProp.Encuesta_pulso)
        .input("Id", sql.Int, leadsCentroProp.Id)
        .query(querys.updateLeadsCentroProp)

        res.json({ isUpdated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

import { getConnection, querys, sql } from "../database"

export const getSeguroGMF = async (req, res) => {
    const { Empresa, Sucursal } = req.body;

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getSeguroGMF)

        res.json(result.recordset)

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}
export const createSeguroGMF = async (req, res) => {
    const { seguroGMF, agencia } = req.body

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, seguroGMF.Fecha)
            .input("Importe_Seguro", sql.Float, seguroGMF.Importe_Seguro)
            .input("Asesor", sql.VarChar, seguroGMF.Asesor)
            .input("Nombre_Cliente", sql.VarChar, seguroGMF.Nombre_Cliente)
            .input("Utilidad", sql.Float, seguroGMF.Utilidad)
            .input("Comision_Asesor", sql.Float, seguroGMF.Comision_Asesor)
            .input("Utilidad_Neta", sql.Float, seguroGMF.Utilidad_Neta)
            .query(querys.createSeguroGMF)

        res.json({ isCreated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}
export const updateSeguroGMF = async (req, res) => {
    const { seguroGMF } = req.body

    try {
        const pool = await getConnection();
        await pool.request()
        .input("Fecha", sql.VarChar, seguroGMF.Fecha)
        .input("Importe_Seguro", sql.Float, seguroGMF.Importe_Seguro)
        .input("Asesor", sql.VarChar, seguroGMF.Asesor)
        .input("Nombre_Cliente", sql.VarChar, seguroGMF.Nombre_Cliente)
        .input("Utilidad", sql.Float, seguroGMF.Utilidad)
        .input("Comision_Asesor", sql.Float, seguroGMF.Comision_Asesor)
        .input("Utilidad_Neta", sql.Float, seguroGMF.Utilidad_Neta)
        .input("Id", sql.Int, seguroGMF.Id)
        .query(querys.updateSeguroGMF)

        res.json({ isUpdated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}
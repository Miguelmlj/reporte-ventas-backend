import { getConnection, querys, sql } from "../database";

// Obtener el inventario físico generado por celular (qrs autos)
export const getInventario = async (req, res) => {
    const { Empresa, Sucursal, Responsable, Fecha } = req.body;
    let Auditor = "N";
    if ( Responsable == 10 ) Auditor = "S";
    
    //crear query para culiacán especifica
    let cuery = querys.getInventario;
    if (Empresa === 5 && Sucursal === 1) cuery = querys.getInventarioZap_Aero;


    try {
        const pool = await getConnection();
        const result = await
            pool.request()
                .input("Empresa", sql.Int, Empresa)
                .input("Sucursal", sql.Int, Sucursal)
                .input("Id_fecha", sql.Date, Fecha)
                .input("Auditor", sql.VarChar, Auditor)
                .query(cuery);
        res.json(result.recordset);

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

}

export const getInventarioSistema = async (req, res) => {
    const { Empresa, Sucursal, Fecha } = req.body;
    let cuery = querys.getInventarioSistema;

    try {
        const pool = await getConnection();
        const result = await
            pool.request()
                .input("Empresa", sql.Int, Empresa)
                .input("Sucursal", sql.Int, Sucursal)
                .query(cuery);
        res.json(result.recordset);

    } catch (error) {
        res.status(500);
        res.send(error.message);

    }

}

export const getInventarioSistemaQr = async (req, res) => {
    const { Empresa, Sucursal } = req.body;
    let cuery = querys.getInventarioSistemaQr;

    try {
        const pool = await getConnection();
        const result = await
            pool.request()
                .input("Empresa", sql.Int, Empresa)
                .input("Sucursal", sql.Int, Sucursal)
                .query(cuery);
        res.json(result.recordset);

    } catch (error) {
        res.status(500);
        res.send(error.message);

    }

}

export const getFechasInventario = async (req, res) => {
    const { Empresa, Sucursal, Fecha, Responsable } = req.body;
    let Auditor = "N";
    //Responsalbe 10 es igual a Auditor
    //Si no es igual enviaremos el inventario de contadores.
    if ( Responsable == 10 ) Auditor = "S";

    let cuery = querys.getFechasInventario;
    /* Se creo la siguiente condición debido a que culiacán maneja el inventario aeropuerto y zapata en la tabla VehiculosDMS e Inventario*/
    if ( Empresa === 5 && Sucursal === 1 ) cuery = querys.getFechasInventarioCuliacan;


    try {
        const pool = await getConnection();
        const result = await
            pool.request()
                .input("Empresa", sql.Int, Empresa)
                .input("Sucursal", sql.Int, Sucursal)
                .input("Auditor", sql.VarChar, Auditor)
                .query(cuery);

        res.json(result.recordset);

    } catch (error) {
        res.status(500);
        res.send(error.message);

    }
}

export const getHistorialComparacionesInventario = async (req, res) => {
    const { Empresa, Sucursal, Responsable, Fecha } = req.body;
    let Auditor = "N";
    if ( Responsable == 10 ) Auditor = "S";

    let cuery = querys.getInventarioHistorial;

    try {
        const pool = await getConnection();
        const result = await
            pool.request()
                .input("Empresa", sql.Int, Empresa)
                .input("Sucursal", sql.Int, Sucursal)
                .input("Id_fecha", sql.Date, Fecha)
                .input("Auditor", sql.VarChar, Auditor)
                .query(cuery);
        res.json(result.recordset);

    } catch (error) {
        res.status(500);
        res.send(error.message);

    }
}

export const inventarioExisteEnHistorial = async (req, res) => {
    const { Empresa, Sucursal, Fecha, Responsable } = req.body;
    let Auditor = "N";
    if ( Responsable == 10 ) Auditor = "S";

    let cuery = querys.inventarioExisteEnHistorial;

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .input("Id_fecha", sql.Date, Fecha)
            .input("Auditor", sql.VarChar, Auditor)
            .query(cuery);

        if (result.rowsAffected > 0) {
            res.json({
                mensaje: "existe"
            })

        } else {
            res.json({
                mensaje: "noexiste"
            })

        }

    } catch (error) {
        res.status(500);
        res.send(error.message);

    }
}

export const inventarioCrearEnHistorial = async (req, res) => {
    const { agencia, data } = req.body;
    const { Empresa, Sucursal, Fecha, Responsable } = agencia;
    let Auditor = "N";
    // let UbicacionDMS = '';
    if ( Responsable == 10 ) Auditor = "S";
    
    try {
        const pool = await getConnection();
        for (const inv of data) {
            await pool.request()
                .input("Empresa", sql.Int, Empresa)
                .input("Sucursal", sql.Int, Sucursal)
                .input("Id_fecha", sql.Date, Fecha)
                .input("Id_vehiculo", sql.VarChar, inv.Id_vehiculo)
                .input("Anio_vehi", sql.VarChar, inv.Anio_vehi)
                .input("Marca", sql.VarChar, inv.Marca)
                .input("Modelo", sql.VarChar, inv.Modelo)
                .input("Serie_sistema", sql.VarChar, inv.Serie_sistema)
                .input("Serie_fisico", sql.VarChar, inv.Serie_fisico)
                .input("Ubicacion", sql.VarChar, inv.Ubicacion)
                .input("Auditor", sql.VarChar, Auditor)
                .input("UbicacionDMS", sql.VarChar, inv.UbicacionDMS)
                .input("QRCapturado", sql.VarChar, inv.QRCapturado)

                .query(querys.crearInventarioEnHistorial)
        }

        res.json({ mensaje: "El Inventario ha sido registrado en historial." })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }


}

export const inventarioEliminarEnHistorial = async (req, res) => {
    const { Empresa, Sucursal, Fecha, Responsable } = req.body;
    let Auditor = "N";
    if ( Responsable == 10 ) Auditor = "S";

    let cuery = querys.eliminarInventarioEnHistorial;

    try {

        const pool = await getConnection();
        await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .input("Id_fecha", sql.Date, Fecha)
            .input("Auditor", sql.VarChar, Auditor)
            .query(cuery);

        res.json({
            mensaje: "eliminado"
        })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

}

/* 
    ENDPOINTS UTILIZADOS PARA MÓDULO QR'S
*/

/* 
export const getInventarioSistemaMochis = async(req, res) => {

}

export const getInventarioSistemaCadillac = async(req, res) => {

}

export const getInventarioSistemaCuliacan = async(req, res) => {

}

export const getInventarioSistemaGuasave = async(req, res) => {

} 

*/

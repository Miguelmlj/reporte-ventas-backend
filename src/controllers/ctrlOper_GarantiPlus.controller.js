import { getConnection, querys, sql } from "../database"

export const getGarantiPlus = async (req, res) => {
    const { Empresa, Sucursal } = req.body;

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getCtrlOper_GarantiPlus)

        res.json(result.recordset) 
        
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

export const createGarantiPlus = async (req, res) => {
    const { GarantiPlus, agencia } = req.body 
    let IsRegisterInBD = false;

    /* try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, GarantiPlus.Fecha)
            .query(querys.ExistsRegister_GarantiPlus)

        if (result.rowsAffected > 0) IsRegisterInBD = true;
        if (result.rowsAffected < 1) { */

            try {
                const pool = await getConnection();
               insertRegisterToBD(pool, GarantiPlus, agencia);
               if (!IsRegisterInBD) res.json({ isCreated: true })

            } catch (error) {
                res.status(500);
                res.send(error.message);
            }

        /* }

        if (IsRegisterInBD) res.json({ isCreated: false })
        if (!IsRegisterInBD) res.json({ isCreated: true })
    } catch (error) {
        res.status(500);
        res.send(error.message);
    } */

}

const insertRegisterToBD = async (pool, GarantiPlus, agencia) => {

    await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, GarantiPlus.Fecha)
            .input("Importe_GarantiPlus", sql.Float, GarantiPlus.Importe_GarantiPlus)
            .input("Costo_GarantiPlus", sql.Float, GarantiPlus.Costo_GarantiPlus)
            .input("Asesor", sql.VarChar, GarantiPlus.Asesor)
            .input("Utilidad", sql.Float, GarantiPlus.Utilidad)
            .input("Comision_Asesor", sql.Float, GarantiPlus.Comision_Asesor)
            .input("Utilidad_Neta", sql.Float, GarantiPlus.Utilidad_Neta)

            .query(querys.createCtrlOper_GarantiPlus)

}

export const updateGarantiPlus = async (req, res) => {
    const { GarantiPlus } = req.body 

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Fecha", sql.VarChar, GarantiPlus.Fecha)
            .input("Importe_GarantiPlus", sql.Float, GarantiPlus.Importe_GarantiPlus)
            .input("Costo_GarantiPlus", sql.Float, GarantiPlus.Costo_GarantiPlus)
            .input("Asesor", sql.VarChar, GarantiPlus.Asesor)
            .input("Nombre_Cliente", sql.VarChar, GarantiPlus.Nombre_Cliente)
            .input("Utilidad", sql.Float, GarantiPlus.Utilidad)
            .input("Comision_Asesor", sql.Float, GarantiPlus.Comision_Asesor)
            .input("Utilidad_Neta", sql.Float, GarantiPlus.Utilidad_Neta)
            .input("Id", sql.Int, GarantiPlus.Id)

            .query(querys.updateCtrlOper_GarantiPlus)

        res.json({ isUpdated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

}
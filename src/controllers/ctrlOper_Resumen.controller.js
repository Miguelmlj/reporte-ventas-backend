import { getConnection, querys, sql } from "../database";
import axios from "axios";

const getLeasing = async (req) => {
    const { Empresa, Sucursal, Fecha } = req;
    const year_month = Fecha.split("-")
    const [periodo, mes] = year_month;
    let leasing = 0;
    let cuery = querys.getLeasingMes;

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("mes", sql.Int, mes)
            .input("periodo", sql.Int, periodo)
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(cuery)

        if (result.rowsAffected > 0) leasing = calcularLeasing(result.recordset);

        // res.json({ leasing: leasing })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

    return leasing;
}

const getPenetracionGMF = async (req) => {
    let { Empresa, Sucursal, Fecha } = req;
    const year_month = Fecha.split("-")
    const [periodo, mes] = year_month;
    let cuery = querys.getPenetracionGMF;
    let p_gmf = 0;
    if (Empresa === 3) Empresa = 2
    if (Empresa === 5) Empresa = 3
    if (Empresa === 7) Empresa = 4

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("mes", sql.Int, mes)
            .input("periodo", sql.Int, periodo)
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(cuery)

        if (result.rowsAffected > 0) p_gmf = Math.round(calcularPenetracionGMF(result.recordset))
        // res.json({ p_gmf: Math.round(p_gmf) });

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

    return p_gmf;
}

export const getUtilidades = async (req, res) => {
    /* NOTAS: precio_felix == sobreprecios GMF, seguros GMF == seguros,precio_felix_contado == sobreprecios contado */
    const { Empresa, Sucursal, Fecha } = req.body;
    const year_month = Fecha.split("-")
    const [periodo, mes] = year_month;

    let parametros = {
        Empresa: Empresa,
        Sucursal: Sucursal,
        Periodo: periodo,
        Mes: mes
    }

    const UtilitiesObject = await requestAsyncUtilities(parametros);
    const FinalResult = reduceUtilitiesLists(UtilitiesObject);
    const leasing = await getLeasing(req.body)
    const penetraciongmf = await getPenetracionGMF(req.body)
    let Totales = sumaTotal(FinalResult);
    Totales = {
        ...Totales,
        contratos_leasing: leasing,
        penetracion_gmf: penetraciongmf
    }
    res.json(Totales)
}

const updateVentasEntregasDMS = async () => {
    try {
        await axios.get("http://fasaip.ddns.net:9080/globaldms/ventasentregas.asp")
    } catch (error) {
        console.log(error);
    }
}

export const getPVAS = async (req, res) => {
    const { Empresa, Sucursal, Fecha, Asesores } = req.body;
    const year_month = Fecha.split("-")
    const [periodo, mes] = year_month;
    let resultados = []
    let parametros = {
        Empresa: Empresa,
        Sucursal: Sucursal,
        Periodo: periodo,
        Mes: mes
    }

    const inactivedSellersList = extractInactivedSellers(Asesores)
    await updateVentasEntregasDMS();
    
    for (const asesor of Asesores) {
        const pvasObject = await requestAsyncPvas(parametros, asesor.Nombre_Asesor, asesor.numvendedor)
        resultados.push(pvasObject)
    }
   
    resultados = filterInactivedSellers(inactivedSellersList, resultados)
   
   const granTotal = sumGranTotal(resultados);
//    resultados.push(granTotal)
   resultados.unshift(granTotal)
   res.json(resultados)
}

const filterInactivedSellers = (inactivedSellersList, resultadosList) => {
    if (inactivedSellersList.length === 0) return resultadosList;

    const newListValidatedSellersNames = selectSellersWithValuesEmpty(inactivedSellersList, resultadosList)
    const finalValidationSellers = extractSellersOfResultadosList(newListValidatedSellersNames, resultadosList)
    return finalValidationSellers;

}

const extractSellersOfResultadosList = (newListValidatedSellersNames, resultadosList) => {
    let finalList = []
    if ( newListValidatedSellersNames.length === 0 ) return resultadosList;

    for (const obj of resultadosList) {
        let isFound = false;
        newListValidatedSellersNames.map((name) => {
            if (name === obj.nombre_asesor[0].nombreAsesor) isFound = true;
        })

        if ( !isFound ) finalList.push(obj)
    }
    return finalList;
}

const selectSellersWithValuesEmpty = (inactivedSellersList, resultadosList) => {
    let SelectedSellersNamesList = []
    const indice = 0;

    for (const seller of inactivedSellersList) {
        resultadosList.map((obj) => {
            if (seller.Nombre_Asesor === obj.nombre_asesor[0].nombreAsesor) {
                if (obj.total_pvas[indice].fact === 0 || obj.total_pvas[indice].cant === 0) SelectedSellersNamesList.push(obj.nombre_asesor[indice].nombreAsesor)
            }
        })
    }

    return SelectedSellersNamesList;
}

const extractInactivedSellers = (Asesores) => {
    const sellers = Asesores.filter(asesor => asesor.Activo !== 'S')
    return sellers;
}

const requestAsyncPvas = async (parametros, nombreAsesor, numVendedor) => {
    /* NOTAS: ge_gmf = garantia extendida gmplus | carprotection = garantia extendida garantiplus */
    let resultsList = {}
    const seguro_gmf_field = "Importe_Seguro"
    const seguro_inbros_field = "Importe_Inbros"
    const ge_gmf_field = "Importe_Garantia"
    const gap_field = "Importe_GAP"
    const onstar_field = "Importe_OnStar"
    const carprotection_field = "Importe_GarantiPlus"
    const accesorios_field = "Importe_Accesorio"
    const precio_felix_field = "Importe_PrecioFelix"
    const precio_felix_contado_field = "Importe_PrecioFelixContado"

    const nombre_asesor = [{ nombreAsesor: nombreAsesor }];
    const seguro_gmf = await getPvaFromMonth(parametros, nombreAsesor, querys.getImporteSeguroFromSeguroGMF)
    const seguro_inbros = await getPvaFromMonth(parametros, nombreAsesor, querys.getImporteInbrosFromSegurosInbros)
    const ge_gmf = await getPvaFromMonth(parametros, nombreAsesor, querys.getImporteGarantiaFromGarantiasGMPlus)
    const gap = await getPvaFromMonth(parametros, nombreAsesor, querys.getImporteGAPFromGAP)
    const onstar = await getPvaFromMonth(parametros, nombreAsesor, querys.getImporteOnStarFromOnStar)
    const carprotection = await getPvaFromMonth(parametros, nombreAsesor, querys.getImporteGarantiPlusFromGarantiPlus)
    const total_pvas = {};
    const accesorios = await getPvaFromMonth(parametros, nombreAsesor, querys.getImporteAccesorioFromAccesorios)
    const precio_felix = await getPvaFromMonth(parametros, nombreAsesor, querys.getImportePrecioFelixFromPrecioFelix)
    const precio_felix_contado = await getPvaFromMonth(parametros, nombreAsesor, querys.getImportePrecioFelixFromPrecioFelixContado)
    const facturacion = await getFacturacionMes(parametros, numVendedor, querys.getFacturacionAsesores)
    const entregas = await getEntregasMes(parametros, numVendedor, querys.getEntregasMes)

    resultsList = {
        nombre_asesor  : nombre_asesor,
        seguro_gmf     : reduceImportsToCantAndFact(seguro_gmf, seguro_gmf_field),
        seguro_inbros  : reduceImportsToCantAndFact(seguro_inbros, seguro_inbros_field),
        ge_gmf         : reduceImportsToCantAndFact(ge_gmf, ge_gmf_field),
        gap            : reduceImportsToCantAndFact(gap, gap_field),
        onstar         : reduceImportsToCantAndFact(onstar, onstar_field),
        carprotection  : reduceImportsToCantAndFact(carprotection, carprotection_field),
        total_pvas     : total_pvas,
        accesorios     : reduceImportsToCantAndFact(accesorios, accesorios_field),
        //precio felix lleva la sumatoria del mismo y precio felix contado.
        precio_felix   : reduceImportsToCantAndFactSumPrecioFelix(precio_felix,precio_felix_contado ,precio_felix_field, precio_felix_contado_field),
        facturacion    : reduceFacturacion( facturacion ),
        entregas       : reduceEntregas( entregas )
    }

    const result_suma_pvas = sumPvas(resultsList)
    resultsList = { ...resultsList, total_pvas: result_suma_pvas }
    return resultsList;
}

const sumPvas = (resultsList) => {
    const fact = resultsList.seguro_gmf[0].fact + resultsList.seguro_inbros[0].fact + resultsList.ge_gmf[0].fact + resultsList.gap[0].fact + resultsList.onstar[0].fact + resultsList.carprotection[0].fact;
    const cant = resultsList.seguro_gmf[0].cant + resultsList.seguro_inbros[0].cant + resultsList.ge_gmf[0].cant + resultsList.gap[0].cant + resultsList.onstar[0].cant + resultsList.carprotection[0].cant;
    const total_sum_pvas = [{ fact: fact, cant: cant }]
    return total_sum_pvas;
}

const sumGranTotal = (list) => {
    const granTotalObject = {
        nombre_asesor: [{ nombreAsesor: "GRAN TOTAL" }],
        seguro_gmf: reduceSumGranTotal(list, "seguro_gmf"),
        seguro_inbros: reduceSumGranTotal(list, "seguro_inbros"),
        ge_gmf: reduceSumGranTotal(list, "ge_gmf"),
        gap: reduceSumGranTotal(list, "gap"),
        onstar: reduceSumGranTotal(list, "onstar"),
        carprotection: reduceSumGranTotal(list, "carprotection"),
        total_pvas: reduceSumGranTotal(list, "total_pvas"),
        accesorios: reduceSumGranTotal(list, "accesorios"),
        precio_felix: reduceSumGranTotal(list, "precio_felix"),
        facturacion: reduceSumGranTotalFacturacion(list),
        entregas: reduceSumGranTotalEntregas(list)
    }
    return granTotalObject;

}

const reduceSumGranTotal = (list, field) => {
    const fact = list.map(row => row[field][0].fact).reduce((prev, curr) => prev + curr, 0)
    const cant = list.map(row => row[field][0].cant).reduce((prev, curr) => prev + curr, 0)
    const result = [{ fact: fact, cant: cant }]
    return result;
}

const reduceSumGranTotalFacturacion = (list) => {
    const facturacion = 'facturacion'
    const gmf = list.map(row => row[facturacion][0].gmf).reduce((prev, curr) => prev + curr, 0)
    const cont = list.map(row => row[facturacion][0].cont).reduce((prev, curr) => prev + curr, 0)
    const bancos = list.map(row => row[facturacion][0].bancos).reduce((prev, curr) => prev + curr, 0)
    const suauto = list.map(row => row[facturacion][0].suauto).reduce((prev, curr) => prev + curr, 0)
    const subt = list.map(row => row[facturacion][0].subt).reduce((prev, curr) => prev + curr, 0)
    const canc = list.map(row => row[facturacion][0].canc).reduce((prev, curr) => prev + curr, 0)
    const total = list.map(row => row[facturacion][0].total).reduce((prev, curr) => prev + curr, 0)
    const result = [{
        gmf: gmf,
        cont: cont,
        bancos: bancos,
        suauto: suauto,
        subt: subt,
        canc: canc,
        total: total
    }]
    return result;
}

const reduceSumGranTotalEntregas = (list) => {
    const entregas = 'entregas'
    const otro = list.map(row => row[entregas][0].otro).reduce((prev, curr) => prev + curr, 0);
    const result = [{otro:otro, tot: otro}]
    return result;
}

const reduceImportsToCantAndFact = (list, field) => {
    // return data.length > 0 ? Math.round(Number(data.map( row => row.Utilidad_Neta).reduce(( prev, curr ) => prev + curr,0).toFixed(2))) : 0
    const fact = list.length > 0 ? Math.round(Number(list.map(row => row[field]).reduce((prev, curr) => prev + curr, 0).toFixed(2))) : 0
    const cant = list.length;
    const result = [{ fact: fact, cant: cant }]
    return result;
}

const reduceImportsToCantAndFactSumPrecioFelix = (precioFelixList, PrecioFelixContadoList, pf_Field, pfc_Field) => {
    // return data.length > 0 ? Math.round(Number(data.map( row => row.Utilidad_Neta).reduce(( prev, curr ) => prev + curr,0).toFixed(2))) : 0
    const pf_fact = precioFelixList.length > 0 ? Math.round(Number(precioFelixList.map(row => row[pf_Field]).reduce((prev, curr) => prev + curr, 0).toFixed(2))) : 0
    const pf_cant = precioFelixList.length;
   
    const pfc_fact = PrecioFelixContadoList.length > 0 ? Math.round(Number(PrecioFelixContadoList.map(row => row[pfc_Field]).reduce((prev, curr) => prev + curr, 0).toFixed(2))) : 0
    const pfc_cant = PrecioFelixContadoList.length;
   
    const result = [{ fact: pf_fact + pfc_fact, cant: pf_cant + pfc_cant }]
    return result;
}

const getPvaFromMonth = async (parametros, nombreAsesor, cuery) => {
    const { Empresa, Sucursal, Periodo, Mes } = parametros;
    let list = []
    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Mes", sql.Int, Mes)
            .input("Periodo", sql.Int, Periodo)
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .input("Asesor", sql.VarChar, nombreAsesor)
            .query(cuery)

        if (result.rowsAffected > 0) list = result.recordset
        return list;

    } catch (error) {
        console.error(error)
        return list;
    }

}

const reduceFacturacion = ( VentasList ) => {
    const numPositivo = 1;
    const numNegativo = -1;
    let gmf = 0;
    let gmfCanc = 0;
    let cont = 0;
    let contCanc = 0;
    let suauto = 0;
    let suautoCanc = 0;
    let bancos = 0;
    let bancosCanc = 0;
    let subt = 0;
    let canc = 0;
    let total = 0;

    const gmfType = 'GMF'
    const contadoType = 'CONTADO'
    const suautoType = 'SUAUTO'
    const bancosType = 'BANCOS'
    const flot_cont_Type = 'FLOT-CONT'
    const flot_gmf_Type = 'FLOT-GMF'
    const flot_bancos_Type = 'FLOT-BANCOS'

    for (const venta of VentasList) {
        if ( venta.TipoVenta === gmfType || venta.TipoVenta === flot_gmf_Type) {
            if (venta.Cant === numPositivo) gmf ++;  
            if (venta.Cant === numNegativo) gmfCanc ++;
            continue;  
        }
        
        if (venta.TipoVenta === contadoType || venta.TipoVenta === flot_cont_Type) {
            if (venta.Cant === numPositivo) cont ++;  
            if (venta.Cant === numNegativo) contCanc ++;
            continue;
        }
        
        if (venta.TipoVenta === suautoType) {
            if (venta.Cant === numPositivo) suauto ++;  
            if (venta.Cant === numNegativo) suautoCanc ++;
            continue;
        }
        
        if (venta.TipoVenta === bancosType || venta.TipoVenta === flot_bancos_Type) {
            if (venta.Cant === numPositivo) bancos ++;  
            if (venta.Cant === numNegativo) bancosCanc ++;
        }
        
    }

    subt = gmf + cont + suauto + bancos;
    canc = gmfCanc + contCanc + suautoCanc + bancosCanc;
    total = subt - canc;

    const result = [{
        gmf: gmf,
        cont: cont,
        bancos: bancos,
        suauto: suauto,
        subt: subt,
        canc: canc,
        total: total
    }]

    return result;
}

const reduceEntregas = ( EntregasList ) => {
    let result = [{otro:0,tot:0}];

    if ( EntregasList.length > 0 ) {
        const totalEnt = EntregasList[0].Cant;
        if ( EntregasList[0].TipoVenta === 'ENTREGA' ) {
            result = [{
                otro: totalEnt,
                tot: totalEnt
            }]
        }
            
    }

    return result;

}

const getFacturacionMes = async (parametros, numVendedor, cuery) => {
    const { Empresa, Sucursal } = parametros;
    let list = []
    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .input("numvendedor", sql.Int, numVendedor)
            .query(cuery)

        if( result.rowsAffected > 0 ) list = result.recordset;
        return list;

    } catch (error) {
        console.error(error)
        return list;
    }
}

const getEntregasMes = async (parametros, numVendedor, cuery) => {
    const { Empresa, Sucursal } = parametros;
    let list = []
    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input('Empresa', sql.Int, Empresa)
            .input('Sucursal', sql.Int, Sucursal)
            .input('numvendedor', sql.Int, numVendedor)
            .query(cuery)

            if ( result.rowsAffected > 0 ) list = result.recordset;
            return list;
    } catch (error) {
        console.log(error);
        return list;
    }
}

const sumaTotal = (FinalResult) => {
// <<<<<<< HEAD
    const { 
        garantia_extendida_gmplus, 
        garantia_extendida_garantiplus, 
        onstar, 
        accesorios, 
        gap, 
        sobreprecios_gmf, 
        seguros_gmf,
        seguros_inbros
    } = FinalResult;
    
    let suma =  {
/* =======
    const { garantia_extendida_gmplus, garantia_extendida_garantiplus, onstar, accesorios, gap, sobreprecios_gmf, seguros } = FinalResult;
    let suma = {
>>>>>>> asesores_activos_select */
        ...FinalResult,
        total: garantia_extendida_gmplus + garantia_extendida_garantiplus + onstar + accesorios + gap + sobreprecios_gmf + seguros_gmf + seguros_inbros
    }
    return suma;
}

const reduceUtilitiesLists = (UtilitiesObject) => {    
    let { 
        garantia_extendida_gmplus, 
        garantia_extendida_garantiplus, 
        onstar, 
        accesorios, 
        gap, 
        sobreprecios_gmf, 
        seguros_gmf,
        seguros_inbros,
        sobreprecios_contado 
    } = UtilitiesObject

    let FinalResult = {
        garantia_extendida_gmplus: reduceUtilidadNeta(garantia_extendida_gmplus),
        garantia_extendida_garantiplus: reduceUtilidadNeta(garantia_extendida_garantiplus),
        onstar: reduceUtilidadNeta(onstar),
        accesorios: reduceUtilidadNeta(accesorios),
        gap: reduceUtilidadNeta(gap),
        sobreprecios_gmf: reduceUtilidadNeta(sobreprecios_gmf),
        seguros_gmf: reduceUtilidadNeta(seguros_gmf),
        seguros_inbros: reduceUtilidadNeta(seguros_inbros),
        sobreprecios_contado: reduceImporte(sobreprecios_contado),
    }

    return FinalResult
}

const reduceUtilidadNeta = (data) => {
    return data.length > 0 ? Math.round(Number(data.map(row => row.Utilidad_Neta).reduce((prev, curr) => prev + curr, 0).toFixed(2))) : 0
}

const reduceImporte = (data) => {
    return data.length > 0 ? Number(data.map(row => row.Importe_PrecioFelixContado).reduce((prev, curr) => prev + curr, 0).toFixed(2)) : 0
}

const requestAsyncUtilities = async (parametros) => {
    let resultsList = {}
    let garantia_extendida_gmplus = await getUtilidad(parametros, querys.getUtilidadNetaGarantiaExtendidaGMPLUS);
    let garantia_extendida_garantiplus = await getUtilidad(parametros, querys.getUtilidadNetaGarantiaExtendidaGarantiPlus);
    let onstar = await getUtilidad(parametros, querys.getUtilidadNetaOnStar);
    let accesorios = await getUtilidad(parametros, querys.getUtilidadNetaAccesorios);
    let gap = await getUtilidad(parametros, querys.getUtilidadNetaGAP);
    let sobreprecios_gmf = await getUtilidad(parametros, querys.getUtilidadNetaSobrePreciosGMF);
    let seguros_gmf = await getUtilidad(parametros, querys.getUtilidadNetaSegurosGMF);
    let seguros_inbros = await getUtilidad(parametros, querys.getUtilidadNetaSegurosInbros);
    let sobreprecios_contado = await getUtilidad(parametros, querys.getImporteSobrePreciosContado);

    resultsList = {
        garantia_extendida_gmplus,
        garantia_extendida_garantiplus,
        onstar,
        accesorios,
        gap,
        sobreprecios_gmf,
        seguros_gmf,
        seguros_inbros,
        sobreprecios_contado
    }
    return resultsList;

}

const getUtilidad = async (parametros, cuery) => {
    const { Empresa, Sucursal, Periodo, Mes } = parametros;
    let list = []
    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Mes", sql.Int, Mes)
            .input("Periodo", sql.Int, Periodo)
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(cuery)

        if (result.rowsAffected > 0) list = result.recordset
        return list;

    } catch (error) {
        console.error(error)
        return list;
    }
}

const calcularPenetracionGMF = (data) => {
    let { contratosgmf, entregas } = data[0];
    return ((contratosgmf / entregas) * 100);
}

const calcularLeasing = (data) => {
    return data.map(row => row.sol_contratos_comprados_leasing).reduce((prev, curr) => prev + curr, 0)
}
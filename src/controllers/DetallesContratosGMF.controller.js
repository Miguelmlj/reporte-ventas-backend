import { getConnection, querys, sql } from "../database"

export const getDetallesContratosGMF = async (req, res) => {
    const { Empresa, Sucursal } = req.body;

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getDetallesContratosGMF)

        res.json(result.recordset)

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

}

export const createDetallesContratosGMF = async (req, res) => {
    const { DetallesContratosGMF, agencia } = req.body
    let IsRegisterInBD = false;

    /* try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, DetallesContratosGMF.Fecha)
            .query(querys.ExistsRegister_DetallesContratosGMF)

        if (result.rowsAffected > 0) IsRegisterInBD = true;
        if (result.rowsAffected < 1) { */

            try {
                const pool = await getConnection();
                insertRegisterToBD(pool, DetallesContratosGMF, agencia);

                if (!IsRegisterInBD) res.json({ isCreated: true })
                
            } catch (error) {
                res.status(500);
                res.send(error.message);
            }

        /* }

        if (IsRegisterInBD) res.json({ isCreated: false })
        if (!IsRegisterInBD) res.json({ isCreated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    } */

}

const insertRegisterToBD = async (pool, DetallesContratosGMF, agencia) => {

    await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, DetallesContratosGMF.Fecha)
            .input("Folio_Contrato", sql.VarChar, DetallesContratosGMF.Folio_Contrato)
            .input("Monto_ContratoAFinanciar", sql.Float, DetallesContratosGMF.Monto_ContratoAFinanciar)
            .input("Nombre_Cliente", sql.VarChar, DetallesContratosGMF.Nombre_Cliente)
            .input("Utilidad", sql.Float, DetallesContratosGMF.Utilidad)

            .query(querys.createDetallesContratosGMF)

}

export const updateDetallesContratosGMF = async (req, res) => {
    const { DetallesContratosGMF } = req.body 

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Fecha", sql.VarChar, DetallesContratosGMF.Fecha)
            .input("Folio_Contrato", sql.VarChar, DetallesContratosGMF.Folio_Contrato)
            .input("Monto_ContratoAFinanciar", sql.Float, DetallesContratosGMF.Monto_ContratoAFinanciar)
            .input("Nombre_Cliente", sql.VarChar, DetallesContratosGMF.Nombre_Cliente)
            .input("Utilidad", sql.Float, DetallesContratosGMF.Utilidad)
            .input("Id", sql.Int, DetallesContratosGMF.Id)

            .query(querys.updateDetallesContratosGMF)

        res.json({ isUpdated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }


}
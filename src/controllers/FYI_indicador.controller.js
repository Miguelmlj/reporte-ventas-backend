import { getConnection, querys, sql } from "../database"

export const createIndicadorFYI = async (req, res) => {
    const { indicadorFYI, agencia } = req.body

    try {
        const pool = await getConnection();
        await pool.request()
            .input("Empresa", sql.Int, agencia.Empresa)
            .input("Sucursal", sql.Int, agencia.Sucursal)
            .input("Fecha", sql.VarChar, indicadorFYI.Fecha)
            .input("GarExtendidas", sql.Int, indicadorFYI.GarExtendidas)
            .input("Seguros", sql.Int, indicadorFYI.Seguros)
            .input("OnStart", sql.Int, indicadorFYI.OnStart)
            .input("ProgValorFactura", sql.Int, indicadorFYI.ProgValorFactura)
            .input("ImporteAccesorios", sql.Float, indicadorFYI.ImporteAccesorios)
            .query(querys.createFYI_indicador)

        res.json({ isCreated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

}

export const getIndicadorFYI = async (req, res) => {
    const { Empresa, Sucursal } = req.body;

    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, Empresa)
            .input("Sucursal", sql.Int, Sucursal)
            .query(querys.getFYI_indicador)

        res.json(result.recordset)

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

}

export const updateIndicadorFYI = async (req, res) => {
    const { indicadorFYI } = req.body

    try {
        const pool = await getConnection();
        await pool.request()
        .input("Fecha", sql.VarChar, indicadorFYI.Fecha)
        .input("GarExtendidas", sql.Int, indicadorFYI.GarExtendidas)
        .input("Seguros", sql.Int, indicadorFYI.Seguros)
        .input("OnStart", sql.Int, indicadorFYI.OnStart)
        .input("ProgValorFactura", sql.Int, indicadorFYI.ProgValorFactura)
        .input("ImporteAccesorios", sql.Float, indicadorFYI.ImporteAccesorios)
        .input("Id_indicador", sql.Int, indicadorFYI.Id_indicador)
        .query(querys.updateFYI_indicador)

        res.json({ isUpdated: true })

    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}
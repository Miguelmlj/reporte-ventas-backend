import express from 'express'
import cors from "cors"
import morgan from "morgan";

import AfluenciaNuevos from "./routes/afluencia_nuevos.routes";
import Citas from "./routes/citas.routes";
import Solicitudes from "./routes/solicitudes.routes";
import Afluencia from "./routes/afluencia.routes";
import Usuarios from "./routes/usuarios.routes";
import Login from "./routes/autenticacion.routes";
import Inicio from "./routes/inicio.routes";
import Anticipo from "./routes/anticipo.routes";
import inventario from "./routes/inventario.routes";
import codigosqr from "./routes/codigosqr.routes"
import FYI_indicador from "./routes/FYI_indicador.routes"
import Leads_indicador from "./routes/Leads_indicador.routes"
import LeadsCentroProp from './routes/Leads_centroProp.routes';
import LeadsJDPower from "./routes/Leads_JDPower.routes";
import BDC_indicador from "./routes/BDC_indicador.routes"
import Mkt_indicador from "./routes/Mkt_indicadores.routes";
import SeguroGMF from "./routes/seguroGMF.routes"
import GarantiasGMPlus from "./routes/garantiasGMPlus.routes"
import OnStar from "./routes/ctrlOper_onStar.routes"
import GAP from "./routes/ctrlOper_GAP.routes";
import Accesorios from "./routes/ctrlOper_Accesorios.routes";
import PrecioFelix from "./routes/ctrlOper_PrecioFelix.routes"
import GarantiPlus from "./routes/ctrlOper_GarantiPlus.routes"
import SegurosInbros from "./routes/ctrlOper_SegurosInbros.routes"
import PrecioFelixContado from "./routes/ctrlOper_PrecioFelixContado.routes"
import DetallesContratosGMF from "./routes/DetallesContratosGMF.routes"
import Asesores from "./routes/asesores.routes"
import ResumenControlOperativoFYI from "./routes/ctrlOper_Resumen.routes"

import config from './config'
import path  from 'path';

const app = express()

// settings
app.set('port', config.port)

// Middlewares
app.use(cors());
app.use(morgan("dev"));
// app.use(express.urlencoded({ extended: false }));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Routes
// app.use("/api", productRoutes);
// reporte diario

//RUTAS PARA TRABAJAR EN SERVIDOR 
/* app.use("/capturas/api", AfluenciaNuevos);
app.use("/capturas/api", Usuarios);
app.use("/capturas/api", Asesores);
app.use("/capturas/api", Citas);
app.use("/capturas/api", Solicitudes);
app.use("/capturas/api", FYI_indicador);
app.use("/capturas/api", Leads_indicador);
app.use("/capturas/api", LeadsCentroProp);
app.use("/capturas/api", LeadsJDPower);
app.use("/capturas/api", BDC_indicador);
app.use("/capturas/api", Mkt_indicador);
app.use("/capturas/api", SeguroGMF);
app.use("/capturas/api", GarantiasGMPlus);
app.use("/capturas/api", OnStar);
app.use("/capturas/api", GAP);
app.use("/capturas/api", Accesorios);
app.use("/capturas/api", PrecioFelix);
app.use("/capturas/api", GarantiPlus);
app.use("/capturas/api", SegurosInbros);
app.use("/capturas/api", PrecioFelixContado);
app.use("/capturas/api", DetallesContratosGMF);
app.use("/capturas/api", Afluencia);
app.use("/capturas/api", Anticipo);
app.use("/capturas/api", inventario);
app.use("/capturas/api", codigosqr);
app.use("/api", Login);
app.use("/capturas/api", Inicio); 
app.use("/capturas/api", ResumenControlOperativoFYI);  */



//RUTAS PARA TRABAJAR EN LOCAL
app.use("/api", AfluenciaNuevos);
app.use("/api", Usuarios);
app.use("/api", Asesores);
app.use("/api", Citas);
app.use("/api", Solicitudes);
app.use("/api", FYI_indicador);
app.use("/api", Leads_indicador);
app.use("/api", LeadsCentroProp);
app.use("/api", LeadsJDPower);
app.use("/api", BDC_indicador);
app.use("/api", Mkt_indicador);
app.use("/api", SeguroGMF);
app.use("/api", GarantiasGMPlus);
app.use("/api", OnStar);
app.use("/api", GAP);
app.use("/api", Accesorios);
app.use("/api", PrecioFelix);
app.use("/api", GarantiPlus);
app.use("/api", SegurosInbros);
app.use("/api", PrecioFelixContado);
app.use("/api", DetallesContratosGMF);
app.use("/api", Afluencia);
app.use("/api", Anticipo);
app.use("/api", inventario);
app.use("/api", codigosqr);
app.use("/api", Login);
app.use("/api", ResumenControlOperativoFYI);
app.use("/api", Inicio);


app.use(express.static(path.join(__dirname, 'public')));

//Manejo de demas rutas: para reconocer las rutas del frontend - react.
app.get( '*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'public/index.html'))
})




export default app